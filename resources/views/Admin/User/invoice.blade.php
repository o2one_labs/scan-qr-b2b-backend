

<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Invoice!</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

</head>
<body>
    <div class="text-center">
        <h4>Formato de recorridos</h4>
    </div>
    
<div style="margin:10px;">
<h6>Fecha: {{$post->datentime}}</h6>
<h6>Nombre {{$post->user_name}}</h6>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col" class="text-center">Punto</th>
      <th scope="col" class="text-center">Condiciones</th>
      <th scope="col" class="text-center">Tiempo</th>
    </tr>
  </thead>
  <tbody style="font-size:14px !important;">
    @foreach($post->postDetails as $posts)
        <tr class="text-center">
          <td>{{$posts->photo}}</td>
          <td>{{$posts->photo_type}}</td>
          <td>{{date("d-m-Y H:i:s",strtotime($posts->created_at))}} </td>
        </tr>
    @endforeach
  </tbody>
</table>
<div>

</body>
</html>