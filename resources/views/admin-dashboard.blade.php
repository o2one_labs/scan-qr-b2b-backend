@extends('layouts.apphome')
@section('content')
<style type="text/css">
    .date-section{
        margin-bottom: 15px;
    }
    .card-icon-bg .card-body .content {

    max-width: 100%!important;
}
.line-height-1
{
    line-height: 40px!important;
}
.graph
{
    padding: 30px!important;
}
</style>
            <div class="breadcrumb">
                <h1 class="mr-2">Version 1</h1>
                <ul>
                    <li><a href="">Dashboard</a></li>
                    <li>Version 1</li>
                </ul>
            </div>
           <!--  <div id="example">
                                        
            </div> -->
           <div class="row date-section">
                <div class="col-md-8"></div>
                <div class="col-md-2">
                    <label for="start_date" class="card-title">From Date</label>
                    <input id="start_date" class="date_from form-control" placeholder="From Date" name="start" value="">
                </div>
                <div class="col-md-2">
                    <label for="end" class="card-title">To Date</label>
                    <input id="end" class="date_to form-control" placeholder="To Date" name="end" value="">
                </div>
             </div>
            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <!-- ICON BG -->
                 <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Financial"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Revenue</p>
                                <p class="text-primary text-24 line-height-1 mb-2">KWD 20</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Add-User"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Users</p>
                                <p class="text-primary text-24 line-height-1 mb-2">10</p>
                            </div>
                        </div>
                    </div>
                </div>

               

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Checkout-Basket"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Restaurant</p>
                                <p class="text-primary text-24 line-height-1 mb-2">22</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Car-2"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Drivers</p>
                                <p class="text-primary text-24 line-height-1 mb-2">11</p>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Money-2"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Ongoing Orders</p>
                                <p class="text-primary text-24 line-height-1 mb-2">07</p>
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Money-2"></i>
                            <div class="content">
                               <p class="text-muted mt-2 mb-0">Complete Orders</p>
                            <p class="text-primary text-24 line-height-1 mb-2">13</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card mb-4">
                        <div class="card-body graph">
                            <div class="card-title">Revenue vs Time</div>
                            <div id="totalSalesInBarChart" style="height: 450px;"></div>
                        </div>
                    </div>
                </div>
                
            </div><br><br>



@endsection