<!DOCTYPE html>
<html lang="en" dir="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Scan</title>
    
    <!-- <link rel="icon" type="image/ico" href="{{asset('image/favicon.png')}}"  sizes="8x8"> -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    
    <link id="gull-theme" rel="stylesheet" href="{{asset('styles/css/themes/lite-purple.min.css')}}">
    <link rel="stylesheet" href="{{asset('styles/vendor/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('styles/vendor/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{asset('styles/vendor/toastr.css')}}">

    <link rel="stylesheet" href="{{asset('styles/vendor/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/dataTables.checkboxes.css')}}">
    <link rel="stylesheet" href="{{asset('css/parsley.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/all.css')}}">
    <link rel="stylesheet" href="{{asset('css/prism.css')}}">
    <link rel="stylesheet" href="{{asset('css/chosen.css')}}">
    <link rel="stylesheet" href="{{asset('styles/vendor/pickadate/classic.css')}}">
    <link rel="stylesheet" href="{{asset('styles/vendor/pickadate/classic.date.css')}}">
    <link rel="stylesheet" href="{{asset('styles/vendor/bootstrap-datepicker.min.css')}}">
    {{--<link rel="stylesheet" href="{{asset('styles/vendor/bootstrap-datetimepicker.min.css')}}">--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}


    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">--}}
{{--    <link rel='stylesheet' id='fontawesome-css' href='https://use.fontawesome.com/releases/v5.0.1/css/all.css?ver=4.9.1' type='text/css' media='all' />--}}


    <link rel="stylesheet" href="{{asset('styles/vendor/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/parsley.css')}}">
     <style>
    .loadscreenProgress{
        text-align: center;
        position: fixed;
        width: 100%;
        left: 0;
        right: 0;
        margin: auto;
        /* top: 50%; */
        height: 100vh;
        background: #fff;
        z-index: 999;
        opacity: 0.5;

    }
     </style>


</head>