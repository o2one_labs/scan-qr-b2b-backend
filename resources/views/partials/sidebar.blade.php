@if(isset($roles[0]))
    @foreach($roles as $roles_rowset)
        @if($roles_rowset->user_type == 0)
            @php $roles_set[] = $roles_rowset;@endphp
        @else
            @php $roles_set[] = null; @endphp
        @endif
    @endforeach
@else
    @php $roles_set[] = null; @endphp
@endif
@php $roles_set = array_filter($roles_set); @endphp
        <div class="side-content-wrap">
            <div class="sidebar-left rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
                <ul class="navigation-left">

                    {{--<li class="nav-item {{ Request::is('home*') ? 'active' : '' }}">
                        <a class="nav-item-hold" href="{{route('home')}}">
                            <i class="nav-icon i-Bar-Chart"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                        <div class="triangle"></div>
                    </li>--}}
                    <li class="nav-item {{ Request::is('admin_post*') ? 'active' : '' }}">
                        <a class="nav-item-hold" href="{{route('admin_post.index')}}">
                            <i class="nav-icon i-Bar-Chart"></i>
                            <span class="nav-text">Posts</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item {{ Request::is('scan*') ? 'active' : '' }}">
                        <a class="nav-item-hold" href="{{route('scan.index')}}">
                            <i class="nav-icon i-Bar-Chart"></i>
                            <span class="nav-text">QR Codes</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    {{--<li class="nav-item {{ Request::is('godeye*') ? 'active' : 'sss' }}">
                        <a class="nav-item-hold" href="{{route('godeye.index')}}">
                            <i class="nav-icon i-Globe"></i>
                            <span class="nav-text">Yum's Eye</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                   <li class="nav-item {{ Request::is('restaurant/*') ? 'active' : 'sss' }}">
                        <a class="nav-item-hold" href="{{route('Restaurent.index')}}">                            <i class="nav-icon i-Computer-Secure"></i>
                            <span class="nav-text">Restaurants</span>
                        </a>
                        <div class="triangle"></div>    
                    </li>
                    <li class="nav-item {{ Request::is('transactions*') ? 'active' : 'sss' }}">
                        <a class="nav-item-hold" href="{{route('transactions.index')}}">
                            <i class="nav-icon i-Data-Transfer"></i>
                            <span class="nav-text">Transactions</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item {{ Request::is('orders*') ? 'active' : 'sss' }}">
                        <a class="nav-item-hold" href="{{route('admin.order')}}">
                            <i class="nav-icon i-Management"></i>
                            <span class="nav-text">Order Management</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item {{ Request::is('item*') ? 'active' : 'sss' }}">
                        <a class="nav-item-hold" href="{{route('item.list')}}">
                            <i class="nav-icon i-Computer-Secure"></i>
                            <span class="nav-text">Item Management</span>
                        </a>
                        <div class="triangle"></div>    
                    </li>

                    <li class="nav-item {{ Request::is('user*') ? 'active' : 'sss' }}">
                        <a class="nav-item-hold" href="{{route('user.index')}}">
                            <i class="nav-icon i-Add-UserStar"></i>
                            <span class="nav-text">User Management</span>
                        </a>
                        <div class="triangle"></div>
                    </li>

                    <li class="nav-item {{ Request::is('notification*') ? 'active' : 'sss' }}">
                        <a class="nav-item-hold" href="{{route('notification.index')}}">
                            <i class="nav-icon i-Bell"></i>
                            <span class="nav-text">Notifications</span>
                        </a>
                        <div class="triangle"></div>
                    </li>

                    <li class="nav-item {{ Request::is('driver*') ? 'active' : 'sss' }}" data-item="uikits3">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Checked-User"></i>
                            <span class="nav-text">Driver Management</span>
                        </a>
                        <div class="triangle"></div>
                    </li>

                    <li class="nav-item {{ Request::is('coupon*') ? 'active' : 'sss' }}" data-item="uikits4">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Tag-3"></i>
                            <span class="nav-text">Coupons</span>
                        </a>
                        <div class="triangle"></div>
                    </li>

                    <li class="nav-item {{ Request::is('roles*') ? 'active' : 'sss' }}"  data-item="uikits2">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Add-User"></i>
                            <span class="nav-text">Role Management</span>
                        </a>
                        <div class="triangle"></div>
                    </li>

                    <li class="nav-item {{ Request::is('settings*') ? 'active' : 'sss' }}"  data-item="uikits">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Loading-3"></i>
                            <span class="nav-text">Setting</span>
                        </a>
                        <div class="triangle"></div>
                    </li>--}}
                    <!--<li class="nav-item" data-item="forms">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-File-Clipboard-File--Text"></i>
                            <span class="nav-text">Forms</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="datatables.html">
                            <i class="nav-icon i-File-Horizontal-Text"></i>
                            <span class="nav-text">Datatables</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item" data-item="sessions">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Administrator"></i>
                            <span class="nav-text">Sessions</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item" data-item="others">
                        <a class="nav-item-hold" href="#">
                            <i class="nav-icon i-Double-Tap"></i>
                            <span class="nav-text">Others</span>
                        </a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-hold" href="http://demos.ui-lib.com/gull-html-doc/" target="_blank">
                            <i class="nav-icon i-Safe-Box1"></i>
                            <span class="nav-text">Doc</span>
                        </a>
                        <div class="triangle"></div>
                    </li> -->
                </ul>
            </div>

            <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
                <!-- Submenu Dashboards -->
                <ul class="childNav" data-parent="dashboard">
                    <li class="nav-item">
                        <a href="dashboard.v1.html">
                            <i class="nav-icon i-Clock-3"></i>
                            <span class="item-name">Version 1</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dashboard.v2.html">
                            <i class="nav-icon i-Clock-4"></i>
                            <span class="item-name">Version 2</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dashboard.v3.html" class="open">
                            <i class="nav-icon i-Over-Time"></i>
                            <span class="item-name">Version 3</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dashboard.v4.html">
                            <i class="nav-icon i-Clock"></i>
                            <span class="item-name">Version 4</span>
                        </a>
                    </li>
                </ul>
                <ul class="childNav" data-parent="forms">
                    <li class="nav-item">
                        <a href="form.basic.html">
                            <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                            <span class="item-name">Basic Elements</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="form.layouts.html">
                            <i class="nav-icon i-Split-Vertical"></i>
                            <span class="item-name">Form Layouts</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="form.input.group.html">
                            <i class="nav-icon i-Receipt-4"></i>
                            <span class="item-name">Input Groups</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="form.validation.html">
                            <i class="nav-icon i-Close-Window"></i>
                            <span class="item-name">Form Validation</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="smart.wizard.html">
                            <i class="nav-icon i-Width-Window"></i>
                            <span class="item-name">Smart Wizard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="tag.input.html">
                            <i class="nav-icon i-Tag-2"></i>
                            <span class="item-name">Tag Input</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="editor.html">
                            <i class="nav-icon i-Pen-2"></i>
                            <span class="item-name">Rich Editor</span>
                        </a>
                    </li>
                </ul>


            </div>
            <div class="sidebar-overlay"></div>
        </div>