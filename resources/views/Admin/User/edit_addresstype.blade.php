                    <form class="needs-validation" id="editformdata" novalidate method="POST" action="{{route('user.update_addresstype', $area->id)}}" enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationTooltip01" >Name<span style="font-size:14px;color:#ff005e;">*</span></label>
                                <input type="text" maxlength="30" class="form-control" id="validationTooltip01" placeholder="Name"name="name_en" value="{{ $area->name_en }}" data-parsley-required-message="Name is required" required>
                                @error('name_en')
                                <p style="color:red;">{{$message}}</p>
                                @enderror

                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationTooltip02 arabic_label"style="float: right;"><span style="font-size:14px;color:#ff005e;">*</span>اسم</label>
                                <input type="text" maxlength="30" class="form-control text-right" id="validationTooltip02" placeholder="اسم" name="name_ar" value="{{ $area->name_ar }}" data-required="true" data-parsley-required-message="مطلوب اسم" required>
                                @error('name_ar')
                                <p style="color:red;">{{$message}}</p>
                                @enderror
                            </div>

                        </div>


                        <button class="btn btn-primary btn-gradient-info mr-2 import_button_label" id="edit_form_submit" type="submit" style="margin-top:10px;">Update</button> <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->

                    </form>

                    <script type="text/javascript">
                        $(document).ready(function(){
                            // form submit
                            $("#edit_form_submit").click(function(event) {
                                if($("#editformdata").parsley().validate()){
                                    $(this).attr("disabled",true);
                                    $('#editformdata').submit();
                                }
                            });
                        });
                    </script>