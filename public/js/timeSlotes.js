$(document).ready(function(){
		$('.hid').hide();
	})

	$(document).ready(function(){
		$(".addData").on('click', function(){
			var id = $(this).data('id');
			$('.tf'+id).show();
			$('.tt'+id).show();
			// $('.tf'+id).attr("required", "true");
			// $('.tt'+id).attr("required", "true");
        	
		})
	})

	$(document).ready(function(){
		$(".clsData").on('click', function(){
			var id = $(this).data('id');
			$('.tf2'+id).hide();
			$('.tt2'+id).hide();
			// $('.tf2'+id).removeAttr('required');
			// $('.tt2'+id).removeAttr('required');
		})
	})
  $(document).ready(function(){
    $('.dayStatus').on('change', function(){
      var id = $(this).data('id');
      var status = $(this).data('id1');
      if($(this).prop('checked'))
      {
        var changeStatus = 1;
        $(".tmFrom"+id).removeAttr('disabled');
        $(".tmTo"+id).removeAttr('disabled');
        $(".tmTo"+id).attr("required", "true");
        $(".tmFrom"+id).attr("required", "true");

      }
      else
      {
        var changeStatus = 0;
        $(".tmFrom"+id).attr('disabled', 'disabled');
        $(".tmTo"+id).attr('disabled', 'disabled');
       $(".tmFrom"+id).removeAttr('required');
        $(".tmTo"+id).removeAttr('required');
        
      }
    });
  }); 

	$(document).ready(function(){
		$(".tf01").on('change', function(){
			var id1 = $(this).data('id');
			$('.tf2'+id1)[0].selectedIndex = 0;
			$('.timeTo2'+id1)[0].selectedIndex = 0;
			$('.timeTo2'+id1).prop("disabled", true);
			$('.tf2'+id1).prop("disabled", true);
			var val = $(this).val();
			var type = 1;
			//alert(id1);
			$.ajaxSetup({
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             }
                        });
            $.ajax({
                    url:"{{route('Restaurant.gettime')}}",
                    method: 'POST',
                    data:{val:val,type:type},
                    type:"json",
                    success:function(response){
                    if(response.status ==1)
                    {
                        $('.timeTo'+id1).prop("disabled", false);
                        var datas = [];
                        datas.push("<option disabled selected value> -- select an option -- </option>")
                        response.time.forEach(r => {
                        datas.push("<option value="+r+">"+r+"</option>")
                            })
                            $('.timeTo'+id1).html(datas);

                    }
                  },
                  error:function()
                  {
                   swal({
                         text: 'Something went wrong',
                         button: {
                                 text: "OK",
                                 value: true,
                                 visible: true,
                                 className: "btn btn-primary"
                                 }
                        })
                  }
            });
		});
		$(".timeFrom22").on('change', function(){
			//var val = $(this).val();
			var id1 = $(this).data('id');
			var val = $(this).val();
			var type = 3;
			var val0 = $(".timeFrom"+id1).val();
			var val1 = $(".timeTo"+id1).val();
			//alert("00:00-"+val0+" "+val+" -24:00");
			$.ajaxSetup({
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             }
                        });
            $.ajax({
                    url:"{{route('Restaurant.gettime')}}",
                    method: 'POST',
                    data:{val:val,val0:val0,val1:val1,type:type},
                    type:"json",
                    success:function(response){
                    if(response.status ==1)
                    {
                        var datas = [];
                        //alert(response.time);
                        datas.push("<option disabled selected value> -- select an option -- </option>")
                        response.time.forEach(r => {
                        datas.push("<option value="+r+">"+r+"</option>")
                            })
                        //alert(datas);
                            $('.timeTo2'+id1).prop("disabled", false);
                            $('.timeTo2'+id1).html(datas);


                    }
                  },
                  error:function()
                  {
                   swal({
                         text: 'Something went wrong',
                         button: {
                                 text: "OK",
                                 value: true,
                                 visible: true,
                                 className: "btn btn-primary"
                                 }
                        })
                  }
            });
		});
		$(".tt01").on('change', function(){
			var id1 = $(this).data('id');
			var val = $(this).val();
			var type = 2;
			var val0 = $(".timeFrom"+id1).val();
			$('.timeTo2'+id1)[0].selectedIndex = 0;
			$('.timeTo2'+id1).prop("disabled", true);
			//alert("00:00-"+val0+" "+val+" -24:00");
			$.ajaxSetup({
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             }
                        });
            $.ajax({
                    url:"{{route('Restaurant.gettime')}}",
                    method: 'POST',
                    data:{val:val,val0:val0,type:type},
                    type:"json",
                    success:function(response){
                    if(response.status ==1)
                    {
                        var datas = [];
                        //alert(response.time);
                        datas.push("<option disabled selected value> -- select an option -- </option>")
                        response.time.forEach(r => {
                        datas.push("<option value="+r+">"+r+"</option>")
                            })
                        //alert(datas);
                            $('.tf2'+id1).prop("disabled", false);
                            $('.tf2'+id1).html(datas);


                    }
                  },
                  error:function()
                  {
                   swal({
                         text: 'Something went wrong',
                         button: {
                                 text: "OK",
                                 value: true,
                                 visible: true,
                                 className: "btn btn-primary"
                                 }
                        })
                  }
            });
		});
		$(".timeTo02").on('change', function(){
			var id = $(this).val();
			//alert(id);
		});
	})


    jQuery(document).ready(function() 
    {

        $(".js-example-tags").select2({
        	tags: true,
        	maximumSelectionLength: 2
        });
    });
    $(window).on('load', function() 
    {
        $("#chosen-select").chosen();
    })
    $(window).on('load', function() 
    {
        $("#chosen-select1").chosen();
    })
    $(window).on('load', function() 
    {
        $("#chosen-select3").chosen({
        	max_selected_options:<?php echo $minmax->min_cuisine; ?>,
        	max_selected_options:<?php echo $minmax->max_cuisine; ?>,
        });
    })