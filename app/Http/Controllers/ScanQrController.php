<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScanQr;

use Validator, DateTime, Mail, DB,Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DataTables;
use Barryvdh\DomPDF\Facade as PDF;

class ScanQrController extends Controller
{
    public function index()
    {
        return view('Admin.ScanQr.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function list(Request $request)
    {
        try { 
            //$data   =   $request->all();
            // $post   = Post::select('id','user_name','datentime','created_at')
            //             ->orderBy('id','asc')->paginate(10);         
            // return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Tag Listing!'),'influencer' =>$post], 200);
        $data = ScanQr::get();
        // dd($data);
        
        return DataTables::of($data)

            ->addColumn('check', function($data){

                    $id = "<input type='checkbox' class='select-related-id' name='check' id='".$data->id."' value='".$data->id."'>";

                    return $id;
                })
            ->addColumn('order_date', function($data){
                    $order_date = date("d-m-Y", strtotime($data->created_at));
                    $order_time = date("h:i a", strtotime($data->created_at));
                    return "<div>".$order_date."</div><div>".$order_time."</div>";

                    //return $order_date;
                })
            ->addColumn('action', function($data) use ($request){
                    $button = "";
                    $button .= '<a type="button" name="edit" id="'.$data->id.'" class="edit btn btn-sm btn-primary" onClick="editScan('.$data->id.')" title="Edit" style="height: 28px;"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>';
                    // $button .= '&nbsp;&nbsp;&nbsp;<a type="button" name="view" id="'.$data->id.'" class="btn btn-info" href="admin_post/invoice/' . $data->id . '" title="View User" style="color:white;height: 28px;">
                    //      <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
                    //      </a>';
                    $button .= '&nbsp;&nbsp;&nbsp;<a type="button" name="delete" data-id="'.$data->id.'" class="btn btn-sm btn-danger delete" title="Order History" style="color:white;height: 28px;">
                         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> 
                         </a>';
                    return $button;
                })


            ->rawColumns(['check','order_date','action'])
            ->make(true);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }       
        
    }

    public function store(Request $request){
    	$scan  = new ScanQr();
    	$scan->name = $request->name;
    	$scan->code = $request->code;
    	$scan->save();
    	return redirect('/scan');
    }

    public function edit(Request $request, $id){
    	$scan  =ScanQr::find($id);
    	return response()->json(['status'=>1,'message'=>'success','data'=>$scan]);
    }

    public function update(Request $request){
    	$scan  =ScanQr::find($request->id);
    	$scan->name = $request->name;
    	$scan->code = $request->code;
    	$scan->save();
    	return redirect('/scan');
    	//return response()->json(['status'=>1,'message'=>'success','data'=>$scan]);
    }

    public function destroy(Request $request, $id)
    {
        ScanQr::where('id',$id)->delete();
        return response()->json(['status'=>1,'message'=>'Success']);
    }
}
