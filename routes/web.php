<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth:web']], function () {
	Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin_post','PostController@index')->name('admin_post.index');
Route::get('admin_post/list','PostController@list')->name('admin_post.list');
Route::get('admin_post/invoice/{id}','PostController@print_invoice')->name('admin_post.print_invoice');
Route::get('admin_post/delete/{id}','PostController@destroy')->name('admin_post.destroy');

Route::get('scan','ScanQrController@index')->name('scan.index');
Route::post('scan','ScanQrController@store')->name('scan.store');

Route::get('scan/edit/{id}','ScanQrController@edit')->name('scan.edit');
Route::post('scan/update','ScanQrController@update')->name('scan.update');
Route::get('scan/list','ScanQrController@list')->name('scan.list');
Route::get('scan/delete/{id}','ScanQrController@destroy')->name('scan.destroy');
});

