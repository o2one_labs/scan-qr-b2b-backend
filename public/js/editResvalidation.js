

            function validateName(){
                            let isFormValidated = false;
             let val = $("#name").val();
             if(val.length === 0){
                $("#error_name_message").text("Name is Required");
                $("#error_name_message").show();
                isFormValidated = false;
            }else{
                $("#error_name_message").hide();
                isFormValidated = true;
            }
            return isFormValidated;
        }
        function validationAr_name(){
                        let isFormValidated = false;
           let val = $("#er_name").val();
           if(val.length === 0){
            $("#error_er_name_message").text("Name is Required");
            $("#error_er_name_message").show();
            isFormValidated = false;
        }else{
            $("#error_er_name_message").hide();
            isFormValidated = true;
        }
        return isFormValidated;
    }

    function validateEmailField(){
                    let isFormValidated = false;
     $("#email-input").value = $("#email-input").val();
     let val = $("#email-input").val();
     var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
     let flag = regex.test(val);
     if(!flag){
         $("#error_email_message").text("Invalid Email Id");
         $("#error_email_message").show();
         isFormValidated = false;
     }else{
         $("#error_email_message").hide();
         isFormValidated = true;
     }
     return isFormValidated;
 }

 function validateLocationNumber()
 {
                let isFormValidated = false;
    $("#location-only").value = $("#location-only").val();
    let val = $("#location-only").val();
    if(val === "")
    {
        $("#error_location_message").text("Please add Your Location.");
        $("#error_location_message").show();
        isFormValidated=false;
    }
    else{
        $("#error_location_message").hide();
        isFormValidated = true;
    }
    return isFormValidated;
}


function validateAddress()
 {
                let isFormValidated = false;
    $("#address").value = $("#address").val();
    let val = $("#address").val();
    if(val === "")
    {
        $("#error_address_message").text("Please add Your Address.");
        $("#error_address_message").show();
        isFormValidated=false;
    }
    else{
        $("#error_location_message").hide();
        isFormValidated = true;
    }
    return isFormValidated;
}

function validatePhoneNumber()
{
   let isFormValidated = false;
   $("#numbers-Only").value = $("#numbers-Only").val();
   let val = $("#numbers-Only").val();
   //var regex = /^[569]\d{7}$/;
   //var regex = /^[0-9]\d{7}$/;
   var regex =/^[0-9]{8,16}$/;
   //var regex = /^[0-9]*$/;
   //let flag = regex.test(val) && (val.length === 8);
   let flag = regex.test(val);
   console.log(flag);
   if(!flag){
       $("#error_mobile_message").text("Contact Number is not valid.");
       $("#error_mobile_message").show();
       isFormValidated = false;
   }else{
       $("#error_mobile_message").hide();
       isFormValidated = true;
   }
   return isFormValidated;
}

function validateManagerPhoneNumber()
{
  //alert("hello");
   let isFormValidated = true;
   $("#managers-numbers-Only").value = $("#managers-numbers-Only").val();
   let val = $("#managers-numbers-Only").val();
   if(val != "")
   {

       var regex =/^[0-9]{8,16}$/;
       let flag = regex.test(val);
       if(!flag)
       {
           $("#error_m_mobile_message").text("Contact Number is not valid.");
           $("#error_m_mobile_message").show();
           isFormValidated = false;
       }else
       {
           $("#error_m_mobile_message").hide();
           isFormValidated = true;
       }

   }
 }


function logovalidate(){
                let isFormValidated = false;
  var file= $("#logo").val();
  var reg = /(.*?)\.(jpg|bmp|jpeg|png)$/;
  if(!file){
    $("#error_file_message").text("Please Select logo.");
    $("#error_file_message").show();
    isFormValidated = false;
}else if(!file.match(reg))
{
    $("#error_file_message").text("Invalid Format");
    $("#error_file_message").show();
    isFormValidated = false;
}else
{
    $("#error_file_message").hide();
    isFormValidated = true;
}
return isFormValidated;
}
function covervalidate(){
                let isFormValidated = false;
  var file= $("#cover_pic").val();
  var reg = /(.*?)\.(jpg|bmp|jpeg|png)$/;
  if(!file){
    $("#error_file1_message").text("Please select Cover Pic.");
    $("#error_file1_message").show();
    isFormValidated = false;
}
else if(!file.match(reg))
{
    $("#error_file1_message").text("Invalid Format.");
    $("#error_file1_message").show();
    isFormValidated = false;
}else
{
    $("#error_file1_message").hide();
    isFormValidated = true;
}
return isFormValidated;
}

function ValidateAdmin(){
                let isFormValidated = false;
     let val = $("#admin_charge").val();
     var regex = /^[0-9]*$/;
    let flag = regex.test(val) && (val.length === 2);
 if(!flag){
    $("#error_admin_message").text("Invalid Admin Commision");
    $("#error_admin_message").show();
    isFormValidated = false;
}else{
    $("#error_admin_message").hide();
    isFormValidated = true;
}
return isFormValidated;
}


// form submit
//    $("input[value=Submit]").click(function(event) {
//      if($("#formdata").parsley().validate()){
//         $(this).attr("disabled",true);
//        $('#formdata').submit();
//     }
//    });
