
<style>
    .toast-info{
        width: 448px !important;
        max-width: 448px !important;
        height: 175px !important;
        font-size: 15px !important;
    }
        .toast-warning {
        width: 360px !important;
        max-width: 600px !important;
        height: 100px !important;
        font-size: 15px !important;
    }
    .toast-warning {
  background-color: #3276b1 !important;
}
    #toast-container>.toast-warning {
        background-image: url()!important;
        padding: 11px 12px 15px 26px !important;
    }
    .reminder{
        font-size: 18px !important;
    }
</style>
        <div class="main-header">
            <div class="logo">
                <img src="{{asset('./images/logo.png')}}" alt="">
            </div>

            <div class="menu-toggle">
                <div></div>
                <div></div>
                <div></div>
            </div>

            <div class="d-flex align-items-center">
                <!-- Mega menu -->
                <div class="dropdown mega-menu d-none d-md-block">
                    <!-- <a href="#" class="btn text-muted dropdown-toggle mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mega Menu</a>
                    <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                        <div class="row m-0">
                            <div class="col-md-4 p-4 bg-img">
                                <h2 class="title">Mega Menu <br> Sidebar</h2>
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Asperiores natus laboriosam fugit, consequatur.
                                </p>
                                <p class="mb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Exercitationem odio amet eos dolore suscipit placeat.</p>
                                <button class="btn btn-lg btn-rounded btn-outline-warning">Learn More</button>
                            </div>


                            <div class="col-md-4 p-4">
                                <p class="text-primary text--cap border-bottom-primary d-inline-block">Features</p>
                                <div class="menu-icon-grid w-auto p-0">
                                    <a href="#"><i class="i-Shop-4"></i> Home</a>
                                    <a href="#"><i class="i-Library"></i> UI Kits</a>
                                    <a href="#"><i class="i-Drop"></i> Apps</a>
                                    <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                                    <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                                    <a href="#"><i class="i-Ambulance"></i> Support</a>
                                </div>
                            </div>
                            <div class="col-md-4 p-4">
                                <p class="text-primary text--cap border-bottom-primary d-inline-block">Components</p>
                                <ul class="links">
                                    <li><a href="accordion.html">Accordion</a></li>
                                    <li><a href="alerts.html">Alerts</a></li>
                                    <li><a href="buttons.html">Buttons</a></li>
                                    <li><a href="badges.html">Badges</a></li>
                                    <li><a href="carousel.html">Carousels</a></li>
                                    <li><a href="lists.html">Lists</a></li>
                                    <li><a href="popover.html">Popover</a></li>
                                    <li><a href="tables.html">Tables</a></li>
                                    <li><a href="datatables.html">Datatables</a></li>
                                    <li><a href="modals.html">Modals</a></li>
                                    <li><a href="nouislider.html">Sliders</a></li>
                                    <li><a href="tabs.html">Tabs</a></li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

                    
                </div>
                <!-- / Mega menu -->
               {{-- <div class="search-bar">
                    <input type="text" placeholder="Search">
                    <i class="search-icon text-muted i-Magnifi-Glass1"></i>
                </div>--}}
            </div>

            <div style="margin: auto"></div>

            <div class="header-part-right">
                <!-- Full screen toggle -->
                <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
                
                <!-- Grid menu Dropdown -->
                <!-- <div class="dropdown">
                    <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="menu-icon-grid">
                            <a href="#"><i class="i-Shop-4"></i> Home</a>
                            <a href="#"><i class="i-Library"></i> UI Kits</a>
                            <a href="#"><i class="i-Drop"></i> Apps</a>
                            <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                            <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                            <a href="#"><i class="i-Ambulance"></i> Support</a>
                        </div>
                    </div>
                </div> -->
                <!-- Notificaiton -->
                <div class="dropdownHead">
                    <div class="badge-top-container" id="dropdownNotificationHead" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="badge badge-primary notify_count"></span>
                        <i class="i-Bell text-muted header-icon"></i>
                    </div>
                    <!-- Notification dropdown -->
                    <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none ps" aria-labelledby="dropdownNotification" data-perfect-scrollbar="" data-suppress-scroll-x="true" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(836px, 58px, 0px);min-width: 375px !important; width: 375px !important;">
                        {{--<div class="dropdown-item d-flex">--}}
                        <div id="notify_rest" style="width: 375px;">
                            <div class="dropdown-item d-flex">
                                <div class="notification-details flex-grow-1">
                                    <p class="m-0 d-flex align-items-center">
                                        <span style="color: blue;font-weight: 600;">Notifications</span><a class="clear_all" style="margin-left: 214px;color: blue;"></a>
                                        <span class="flex-grow-1"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        {{--</div>--}}
                    </div>
                </div>
                <audio id="buzzer" src="{{asset('/audio/buzz.mp3')}}" type="audio/mp3"></audio>
                <input type="button" value="Start" id="start" style="display: none;"/>
                <!-- Notificaiton End -->



                <!-- User avatar dropdown -->
                <div class="dropdown">
                    <div class="user col align-self-end" id="profile-section">
                        <img src="{{asset('./image/favicon.png')}}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <!-- <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i> {{Auth::user()->name}}
                            </div> -->
                            <!-- <a class="dropdown-item">Account settings</a> -->
                            <!-- <a class="dropdown-item">Billing history</a> -->
                            {{--<a class="dropdown-item" href="{{route('home.changePassword')}}">Change Password</a>--}}
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                        </div>
                    </div>
                </div>





                {{--<div class="dropdown">--}}
                    {{--<div class="user col align-self-end">--}}
                        {{--<img src="{{asset('assets/images/faces/1.jpg')}}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">--}}
                        {{--<!-- <div class="dropdown-header">--}}
                                {{--<i class="i-Lock-User mr-1"></i> {{Auth::user()->name}}--}}
                                {{--</div> -->--}}
                            {{--<a class="dropdown-item">Account settings</a>--}}
                            {{--<a class="dropdown-item">Billing history</a>--}}


                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


            </div>

        </div>
        <script src="{{asset('js/vendor/jquery-3.3.1.min.js')}}"></script>
        <script src="https://js.pusher.com/5.1/pusher.min.js"></script>
        <script src="{{asset('js/toastr.script.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.28/moment-timezone-with-data.js"></script>
        <script>
            $('#userDropdown').click(function () {
                $(".align-self-end").addClass("open");
            });
            $('#dropdownNotificationHead').click(function () {
                $(".dropdownHead").addClass("open");
            })
        </script>

