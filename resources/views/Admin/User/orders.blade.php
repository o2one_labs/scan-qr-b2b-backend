@extends('layouts.apphome')
@section('content')

            <div class="breadcrumb">
                <h1>Datatables</h1>
                <ul>
                    <li><a href="">UI Kits</a></li>
                    <li>Datatables</li>
                </ul>
            </div>
            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-6">
                    <h4><a class="back_arrow" href="javascript: history.go(-1)">
                        <img src="{{asset('image/left-arrow.png')}}">
                    </a>{{$user->name_en}} Orders Listing</h4>

                    <style type="text/css">
                      .button_r
                      {
                        width: 150px;
                        float: right;
                        margin-right: 20px;
                        margin-top: 7px;
                      }
      
                    </style>
                  </div>
                {{--<div class="col-md-6 col-sm-12 col-xs-12 text-right">
                    <div class="button_list">
                        <ul>
                            <li>
                                <select name="deliver" id="deliver" class="form-control deliver custom_dropdown" required>
                                    <option disabled selected value class="form-control"> Select </option>
                                    <option value="inactive" data-id1="Inactiveted">Block</option>
                                    <option value="active" data-id1="Activated">Unblock</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                </div>--}}
                 

            </div>
            <div class="row">

<div class="col-md-12 mb-4">

    <div class="card text-left">

        <div class="card-body">

            <div class="table-responsive" style="width: 100%;">
                    <table id="user_table" class="table table-bordered table-striped display nowrap">
                     <thead>
                      <tr>
                        {{--<th><input type="checkbox" class="checkall" id="0"></th>--}}
                        <th>Order IDs</th>
                        <th>Order Date</th>
                        <th>Restaurant</th>
                        <th>Order Status</th>
                        <th>Payment Mode</th>
                        <th>Payment Status</th>
                        <th>Total Amount (in KD)</th>
                        <th>Action</th>

                      </tr>
                     </thead>
                    </table>
            </div>

        </div>
    </div>
</div>

</div>

@endsection

@section('scripts')
<script src="{{asset('js/vendor/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/dataTables.checkboxes.min.js')}}"></script>
<script src="{{asset('js/dataTables.checkboxes.js')}}"></script>
<scrip>


<script>

$(document).ready(function(){

    $('.checkall').prop('checked',false);
    // $("#deliver")[0].selectedIndex = 0;
    // $('#deliver').hide();

    var user_id = '<?php echo $user->id; ?>';

    query_string = '?id='+user_id;
 $('#user_table').on( 'init.dt', function () {
     //$("#user_table thead tr th:first").removeClass('sorting_asc');
 } ).DataTable({
  processing: true,
  serverSide: true,
  "order": [[ 1, "desc" ]], //Initial no order.
  "aaSorting": [],
  ajax: {
   url: "{{ route('user.orders',$user->id) }}",
   columnDefs : [{"targets":[],"orderable": false,"type":"date-eu",className: 'text-center'}], 
  },

   columns: [
   {
    data: 'order_id',
    name: 'order_id',
    orderable: false,
    "width" : "20%",
    },
    {   data: 'created_at', name: 'created_at', orderable: true, "width" : "20%","sType":"date" , sort: 'timestamp'},
    {
    data: 'restaurant_name',
    name: 'restaurant_name',
    orderable: true,
    "width" : "20%",
    },
   {
       data: 'order_status',
       name: 'order_status',
       orderable: false,
       "width" : "20%",
   },
   {
       data: 'payment_mode',
       name: 'payment_mode',
       orderable: false,
       "width" : "20%",
   },
   {
       data: 'payment_status',
       name: 'payment_status',
       orderable: false,
       "width" : "20%",
   },
    {
    data: 'overall_amount',
    name: 'overall_amount',
    orderable: false,
    "width" : "20%",
    },
  {
    data: 'action',
    name: 'action',
    orderable: false,
    "width" : "10%",
  }
  ]
 });

    $('.checkall').click(function(){
        if(this.checked){
            $('.select-related-id').prop('checked',true);
        }else{
            $('.select-related-id').prop('checked',false);
        }
    });  $(document).on('click','.select-related-id',function(){
        if($('.checkall').is(':checked') && !this.checked){
            $('.checkall').prop('checked',false);
        }
        else if($('.select-related-id').length == $('.select-related-id:checked').length){
            $('.checkall').prop('checked',true);
        }
    });

    /*$("#deliver").change(function()
    {
        var type=2;
        var deliver = $(this).val();
        // if(deliver =="inactive"){
        //   var msgdel = "Area inactive ";
        // }else if(deliver =="active"){
        //
        // }else{
        //
        // }
        var someObj = {};
        someObj.fruitsGranted = [];
        someObj.fruitsDenied = [];
        $("input:checkbox").each(function() {
            if ($(this).is(":checked")) {
                someObj.fruitsGranted.push($(this).attr("id"));
            } else {
                someObj.fruitsDenied.push($(this).attr("id"));
            }
        });
        var sel = someObj.fruitsGranted;
        //alert(sel.length);

        if(sel.length >0)
        {
            $.ajaxSetup({headers : { "X-CSRF-TOKEN" :jQuery(`meta[name="csrf-token"]`). attr("content")}});
            $.ajax({
                url: "{{route('user.update')}}",
                method: 'POST',
                data:{sel:sel,deliver:deliver,type:type},
                type:"json",
                success: function(response)
                {
                    if(response.status == 'success')
                    {
                        swal("Success!", "User has been updated!", "success");
                        //swal("Updated!", "Restaurent has been Updated", "success");
                        $("#deliver")[0].selectedIndex = 0;
                        $('.checkall').prop('checked',false);
                        $('#user_table').DataTable().ajax.reload();
                    }
                },
                error:function()
                {
                    swal({
                        text: 'Something went wrong',
                        button:{
                            text: "OK",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    })
                }
            });
        }
        else
        {
            swal({
                // title:"Something Went wrong",
                text: 'You have not selected any User',
                button: {
                    text: "OK",
                    value: true,
                    visible: true,
                    className: "btn btn-primary"
                }
            })
        }


    });*/

    $(document).on('click', '.status', function(){
        id = $(this).data('id');
        st = $(this).data('id1');
        type = 1;
        if(st == 1)
        {
            act = "Block";
        }
        else
        {
            act = "Unblock";
        }
        swal({
            title: 'Are you sure?',
            text: "You want to "+act,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3f51b5',
            cancelButtonColor: '#ff4081',
            confirmButtonText: 'OK ',
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "OK",
                    value: true,
                    visible: true,
                    className: "btn btn-info",
                    closeModal: true,
                }
            }

        }).then(function(isConfirm){
            if(isConfirm){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url:"{{route('user.update')}}",
                    method: 'POST',
                    data:{id:id,st:st,type:type},
                    type:"json",
                    success: function(data) {
                        if(st == 1)
                        {
                            swal("Success!", "User has been Blocked", "success");
                        }
                        else
                        {
                            swal("Success!", "User has been Unblocked", "success");

                        }
                        $('.checkall').prop('checked',false);
                        $('#user_table').DataTable().ajax.reload();
                    },
                    error:function(){
                        swal({
                            text: 'Something went wrong',
                            button: {
                                text: "OK",
                                value: true,
                                visible: true,
                                className: "btn btn-primary"
                            }
                        })
                    }
                });
            }
        })

    });

});
</script>
@endsection