@extends('layouts.apphome')
@section('content')
    <style>
        thead tr th:first-child { width: 1px !important;}

        .parsley-required {
            color: red;
        }

        #parsley-id-7 {
            direction: rtl;
            float: right;
        }
    </style>

            <div class="breadcrumb">
                <h1>Datatables</h1>
                <ul>
                    <li><a href="">UI Kits</a></li>
                    <li>Datatables</li>
                </ul>
            </div>
            <div class="separator-breadcrumb border-top"></div>
            <div class="modal" id="myModal">
                <div class="modal-dialog modal-dialog-scrollable modal-lg">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h3 class="modal-title">Add Address Type</h3>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form class="needs-validation" id="formdata" novalidate method="POST" action="{{route('user.create_addresstype')}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
                                <div class="form-row">
                                    <div class="col-md-6 mb-2">
                                        <label for="">Name<span style="font-size:14px;color:#ff005e;">*</span></label>
                                        <input type="text" id="gov_name_en" multiple class="form-control @error('name_en') is-invalid @enderror" placeholder="Name" name="name_en" data-required="true" data-parsley-required-message="Name is required" required>
                                    </div>
                                    <div class="col-md-6 mb-2">
                                        <label for="arabic_label"   style="float: right;"><span style="font-size:14px;color:#ff005e;">*</span>اسم</label>
                                        <input type="text" multiple class="form-control text-right @error('name_ar') is-invalid @enderror" id="gov_name_ar" placeholder="اسم" name="name_ar" data-required="true" data-parsley-required-message="مطلوب اسم" required>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-gradient-info mr-2 import_button_label" id="form_submit" type="submit" style="margin-top:22px;">Submit</button> <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal" id="myModal2">
                <div class="modal-dialog modal-dialog-scrollable modal-lg">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h3 class="modal-title">Update Address Type</h3>
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-6">


                    <h4>Address Types</h4>

                    <style type="text/css">
                      .button_r
                      {
                        width: 150px;
                        float: right;
                        margin-right: 20px;
                        margin-top: 7px;
                      }
      
                    </style>
                  </div>
                <div class="col-md-6 col-sm-12 col-xs-12 text-right">
                    <div class="button_list">
                        <ul>
                            <li>
                                <select name="deliver" id="deliver" class="form-control deliver custom_dropdown" required>
                                    <option disabled selected value class="form-control"> Select </option>
                                    <option value="inactive" data-id1="Inactiveted">Inactive</option>
                                    <option value="active" data-id1="Activated">Active</option>
                                </select>
                            </li>
                            <li> <a type="button" class="custom_dropdown add" data-toggle="modal" data-target="#myModal" style="cursor: pointer;">Add Address Type</a>
                            </li>
                        </ul>
                    </div>
                </div>
                 

            </div>
            <div class="row">

<div class="col-md-12 mb-4">

    <div class="card text-left">

        <div class="card-body">

            <div class="table-responsive" style="width: 100%;">
                    <table id="user_table" class="table table-bordered table-striped display nowrap">
                     <thead>
                      <tr>
                        <th><input type="checkbox" class="checkall" id="0"></th>
                        <th>Name</th>
                        <th>اسم</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                     </thead>
                    </table>
            </div>

        </div>
    </div>
</div>

</div>

@endsection

@section('scripts')
<script src="{{ asset('js/parsley.js')}}"></script>
<script src="{{asset('js/vendor/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/dataTables.checkboxes.min.js')}}"></script>
<script src="{{asset('js/dataTables.checkboxes.js')}}"></script>
<script>

    $(document).ready(function(){
        // form submit
        $("#form_submit").click(function(event) {
            if($("#formdata").parsley().validate()){
                $(this).attr("disabled",true);
                $('#formdata').submit();
            }
        });
    });

    $(window).on('load', function() {
        $('.dropdown-item').click(function (e) {
            e.stopPropagation();
        });
    });

$(document).ready(function(){

    $('.checkall').prop('checked',false);
    $("#deliver")[0].selectedIndex = 0;


 table = $('#user_table').on( 'init.dt', function () {
     $("#user_table thead tr th:first").removeClass('sorting_asc');
 } ).DataTable({
  processing: true,
  serverSide: true,
  orderable: false,
  ajax: {
   url: "{{ route('user.address_type') }}",
  },

   columns: [
   {
   data: 'check',
   name: 'check',
   orderable: false,
   "width":"1px",
   },
   {
    data: 'name_en',
    name: 'name_en',
    orderable: true,
    width : "25%",
    },
   {
       data: 'name_ar',
       name: 'name_ar',
       orderable: false,
       width : "25%",
   },
   {
   data: 'status',
   name: 'status',
   orderable: false,
   width : "25%",
   },
  {
    data: 'action',
    name: 'action',
    orderable: false,
      width : "25%",
  }
  ]
 });

    $('.checkall').click(function(){
        if(this.checked){
            $('.select-related-id').prop('checked',true);
        }else{
            $('.select-related-id').prop('checked',false);
        }
    });  $(document).on('click','.select-related-id',function(){
        if($('.checkall').is(':checked') && !this.checked){
            $('.checkall').prop('checked',false);
        }
        else if($('.select-related-id').length == $('.select-related-id:checked').length){
            $('.checkall').prop('checked',true);
        }
    });

    $("#deliver").change(function()
    {
        var type=3;
        var deliver = $(this).val();
        // if(deliver =="inactive"){
        //   var msgdel = "Area inactive ";
        // }else if(deliver =="active"){
        //
        // }else{
        //
        // }
        var someObj = {};
        someObj.fruitsGranted = [];
        someObj.fruitsDenied = [];
        $("input:checkbox").each(function() {
            if ($(this).is(":checked")) {
                someObj.fruitsGranted.push($(this).attr("id"));
            } else {
                someObj.fruitsDenied.push($(this).attr("id"));
            }
        });
        var sel = someObj.fruitsGranted;
        //alert(sel.length);

        if(sel.length >0)
        {
            $.ajaxSetup({headers : { "X-CSRF-TOKEN" :jQuery(`meta[name="csrf-token"]`). attr("content")}});
            $.ajax({
                url: "{{route('user.update')}}",
                method: 'POST',
                data:{sel:sel,deliver:deliver,type:type},
                type:"json",
                success: function(response)
                {
                    if(response.status == 'success')
                    {
                        swal("Success!", "Address Type has been updated!", "success");
                        //swal("Updated!", "Restaurent has been Updated", "success");
                        $("#deliver")[0].selectedIndex = 0;
                        $('.checkall').prop('checked',false);
                        $('#user_table').DataTable().ajax.reload();
                    }
                },
                error:function()
                {
                    swal({
                        text: 'Something went wrong',
                        button:{
                            text: "OK",
                            value: true,
                            visible: true,
                            className: "btn btn-primary"
                        }
                    })
                }
            });
        }
        else
        {
            $("#deliver")[0].selectedIndex = 0;
            swal({
                // title:"Something Went wrong",
                text: 'You have not selected any Address Type',
                button: {
                    text: "OK",
                    value: true,
                    visible: true,
                    className: "btn btn-primary"
                }
            })
        }


    });

});

    $(document).on('click', '.load-ajax-modal',function(){

        user_id = $(this).attr('id');

        $.ajax({
            url: "{{route('user.addresstype_edit')}}",
            method: 'GET',
            data:{id:user_id},
            type:"json",
            success: function(response) {
                if(response.status == 'success'){
                    $('#myModal2 div.modal-body').html(response.data);
                }
            },
            error:function(){
                swal({
                    text: 'Something went wrong',
                    button: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn btn-primary"
                    }
                })
            }
        });
    });
</script>
@endsection