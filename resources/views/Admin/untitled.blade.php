public function editPost(Request $request,$id)
    {
      if ($request->isMethod('post')) {
        //dd($request->all());
          $request->validate([
             // 'name_en' => 'required|max:100',
             // 'is_active' => 'required',
          ]);
          $post_image ='';
          if($request->hasFile('post_image')){

             foreach($request->post_image as $file){
               $rand = mt_rand(000,999);
               $extension = $file->getClientOriginalExtension();
               $name = time().$rand.'-bahri.'.$extension;
               $filePath = 'post/'.$name;

               $image = Image::make($file)->resize(700, 600);
               //dd($image);
               Storage::disk('s3')->put($filePath, $image->stream());
              // Storage::disk('s3')->put($filePath, file_get_contents($file));
               $fileNameToStore[] =$filePath;
             }
             $post_image =   implode(",",$fileNameToStore);
          }
          $coverImage='';
          if ($request->hasFile('cover_image')) {
              $rand1 = mt_rand(000,999);
              $file = $request->file('cover_image');
              $extension = $request->file('cover_image')->getClientOriginalExtension();
              $name = time().$rand1.'-bahri.'.$extension;
              $filePath = 'post/'.$name;

                $image = Image::make(request()->file('cover_image'))->resize(700, 600);
                //dd($image);
                Storage::disk('s3')->put($filePath, $image->stream());


              //Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
              $coverImage =$filePath;
            }
            $post   = Post::with('users')->where('id',$id)->first();
            if($coverImage == "")
            {
              $coverImage = $post->cover_pic;
            }
            if($post_image == "")
            {
              $post_image = $post->post_image;
            }
            $devices = Device::where('user_id', $post->user_id)->where('notificationSwitch',1)->get(); 
            $notificationer = new Notificationer;
            if($post->user_type == 3)
            {
              $admin = AdminContact::find(1);
              $username = $admin->name_en;
             // dd($username);
            } 
            else
            {
              $username = $post->users->name_en;
            }        
            
            $post_title = $post->post_title_en;
            $post->post_image=$post_image;
            $post->cover_pic=$coverImage;
             $post->post_title_en = isset($request->post_title_en)?$request->post_title_en:'';
             $post->post_title_ar = isset($request->post_title_ar)?$request->post_title_ar:'';

            // $post->product_service_en = isset($request->product_service_en)?$request->product_service_en:'';
             //$post->product_service_ar = isset($request->product_service_ar)?$request->product_service_ar:'';
            if(!empty($request->attrKey))
           {
            $attributes = [];
            $a_en = [];
            $a_ar = [];
             for($i=0; $i<sizeof($request->attrKey); $i++)
             {
              $att = explode('/',$request->attrKey[$i]);
              $attr = $att[0]."=".$request->attrVal[$i];
              $attr_en = $att[0]."=".$request->attrVal[$i];
              $attr_ar = $att[1]."=".$request->attrVal[$i];
              array_push($attributes, $attr);
              array_push($a_en, $attr_en);
              array_push($a_ar, $attr_ar);
             } 
             $attributes = implode(',',$attributes);
             $a_en = implode(',',$a_en);
             $a_ar = implode(',',$a_ar);
           }
             $post->state_id = $request->state_id;
             $city = explode(',',$request->city_id);
             $city_id = $city[0];
             $city_name = $city[1];
          //   dd($city_name);
             $post->city_id = $city_id;
             $post->location_en = $city_name;
             $post->location_ar = $city[2];
             $post->category_id= isset($request->category_id)?$request->category_id:$post->category_id;
             $post->sub_category_id = isset($request->sub_category_id)?$request->sub_category_id:$post->sub_category_id;
             $post->external_attributes = isset($attributes)?$attributes:'';
             $post->external_attributes_en = isset($a_en)?$a_en:'';
             $post->external_attributes_ar = isset($a_ar)?$a_ar:'';
             $post->description_en = isset($request->descritption_en)?$request->descritption_en:$post->description_en;
             $post->description_ar = isset($request->descritption_ar)?$request->descritption_ar:$post->description_ar;
             $post->price = isset($request->price)?$request->price:0;
             $post->currency_code = $request->currency;

//dd($post_image);
           $post->save();
           if($post->user_type != 3)
           {
                //$message = 'Post has been approved successfully';

          $action = "Updated";
          //dd($username, $post->users->email);
          Mail::to($post->users->email)->send(new Postapproved($username, $post_title, $action));

//*****************FCM Push Notification to Company or User for disapproving Post*******************//
                //$deviceToken = 'dmbYN4ZGgok:APA91bFJpbSdgaS38nQe6CL8aJGUYwxsR5SPeEy2uGCient-hBPkdHWgBC8T_0Yq-WfyYSIGaPWFHdsZiMv5CE5q3tXEOvPTZTUGA95OjvszTuVpgbhxUW-u_OWpF84NK4vgkJhS4dHi';
                

               //Following Variable is used for FCM Push Notification.............

                
                $user_type = $post->users->user_type;
                $post_id = $post->id;
                $user_id =$post->user_id;
                $image = env('AWS_URL').$post->cover_pic;
                $type = 4;
                $is_verifiy = $post->is_verifiy;
                // $titles = "title_".app()->getLocale();
                // $messages = "messages_".app()->getLocale();
                $title_en ="Post Updated";
                $title_ar =  "تم تعديل الإعلان";
                $messages_en = 'Your Post '.$post->post_title_en.' has been Updated by Administrator';
                $messages_ar = 'تم التعديل على إعلان '.$post->post_title_ar;
                //$title = "Post Updated";
                //$messages = 'Your Post '.$post->post_title_en.' has been Updated by Administrator';
                foreach($devices as $device)
                {
                  $title = "title_".$device->lang;
                  $message = "messages_".$device->lang;
                  $deviceToken = $device->device_token;
                  $device_type = $device->device_type;
               $response = $this->pushNotification($type, $deviceToken, $$title, $$message, $image, $user_id, $user_type, $post_id, $is_verifiy, $device_type);
                }
                $notificationer->notification_type = $type;
                $notificationer->user_id=$user_id;
                $notificationer->post_id=$post_id;
                $notificationer->image=$post->cover_pic;
                $notificationer->title_en=$title_en;
                $notificationer->title_ar=$title_ar;
                $notificationer->message_en=$messages_en;
                $notificationer->message_ar=$messages_ar;
                $notificationer->save();

          return redirect('/admin/post/')->with('message', 'Post has been updated succesfully !');

           }
           else
           {
              return redirect('admin/post/myPost')->with('message', 'Post has been updated succesfully !');
           }

      }
      $post = Post::where('id',$id)->first();
          $category = Category::where('is_active', 1)->where('is_delete', 0)->where('parent_id', 0)->get();

          //dd($subcategory);
          $states = State::where('is_active', 1)->where('is_delete', 0)->get();
          $city = City::where('is_active', 1)->where('is_delete', 0)->where('state_id',$post->state_id)->get();
          
          $atrbts = Category::select('attributes_en','attributes_ar')->where('id',$post->sub_category_id)->first();
          $subcategory = Category::where('is_active', 1)->where('is_delete', 0)->where('parent_id', $post->category_id)->get();
         //dd(json_decode($atrbts->attributes_ar));
      return view('pages.Admin.post.editpost',compact('post','city', 'states', 'subcategory', 'category','atrbts'));
    }