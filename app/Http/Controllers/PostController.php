<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostDetail;
use Illuminate\Http\Request;

use Validator, DateTime, Mail, DB,Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DataTables;
use Barryvdh\DomPDF\Facade as PDF;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.User.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function list(Request $request)
    {
        try { 
            //$data   =   $request->all();
            // $post   = Post::select('id','user_name','datentime','created_at')
            //             ->orderBy('id','asc')->paginate(10);         
            // return response(['success' => 1, 'statuscode' => 200, 'msg' => __('Tag Listing!'),'influencer' =>$post], 200);
        $data = Post::get();
        
        return DataTables::of($data)

            ->addColumn('check', function($data){

                    $id = "<input type='checkbox' class='select-related-id' name='check' id='".$data->id."' value='".$data->id."'>";

                    return $id;
                })
            ->addColumn('order_date', function($data){
                    $order_date = date("d-m-Y", strtotime($data->created_at));
                    $order_time = date("h:i a", strtotime($data->created_at));
                    return "<div>".$order_date."</div><div>".$order_time."</div>";

                    //return $order_date;
                })
            ->addColumn('action', function($data) use ($request){
                    $button = "";
                    // $button .= '<a type="button" name="edit" id="'.$data->id.'" class="edit btn btn-primary" title="Edit" href="edit/' . $data->id . '" style="height: 28px;"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>';
                    $button .= '&nbsp;&nbsp;&nbsp;<a type="button" name="view" id="'.$data->id.'" class="btn btn-info" href="admin_post/invoice/' . $data->id . '" title="View User" style="color:white;height: 28px;">
                         <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
                         </a>';
                    $button .= '&nbsp;&nbsp;&nbsp;<a type="button" name="delete" data-id="'.$data->id.'" class="btn btn-danger delete" title="Order History" style="color:white;height: 28px;">
                         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> 
                         </a>';
                    return $button;
                })


            ->rawColumns(['check','order_date','action'])
            ->make(true);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }       
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{ 
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'datentime' => 'required',
                ]
            ); 
            if ($validation->fails()) {
                return response(array('success' => 0, 'statuscode' => 400, 'msg' =>
                    $validation->getMessageBag()->first()), 400);
            } 

            $post = new Post();
            $post->user_name = $request->name;
            $post->datentime = $request->datentime;
            $post->save();
            foreach($request->photo as $key=>$value){
                $postDetail = new PostDetail();
                $postDetail->post_id = $post->id;
                $postDetail->photo = $value;
                $postDetail->photo_type = $request->photo_type[$key];
                $postDetail->comments = $request->comments[$key];
                $postDetail->save();
            }
            return response(['success' => 1, 'statuscode' => 200, 'msg' =>"Success"], 200);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Post::with('postDetails')->where('id',$id)->delete();
        PostDetail::where('post_id',$id)->delete();
        return response()->json(['status'=>1,'message'=>'Success']);
    }

    public function print_invoice(Request $request, $id) {

        
        $post = Post::with('postDetails')->find($id);
        $data = [
            "title" => "Order Invoice",
            "post" => $post
        ];
      // Mail::to('palak@o2onelabs.com')->send(new OrderEmail($order_details));

        //return view('Admin.User.invoice',compact('post'));
       $pdf = PDF::loadView('Admin.User.invoice', $data);
       //return $pdf->download('invoice.pdf');
        return $pdf->stream('invoice.pdf');
    }
}
