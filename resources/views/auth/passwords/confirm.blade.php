
<!DOCTYPE html>    
<html lang="en" dir="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Yum</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('styles/css/themes/lite-purple.min.css')}}">
</head>

<body class="text-left">
    <div class="auth-layout-wrap" style="background-image: url({{asset('images/yum_background.jpg')}});background-repeat: no-repeat;">
        <div class="auth-content" style="margin-left: 70px;">
            <div class="card o-hidden" style="background-color: transparent !important;border: 1px solid white !important;">
                <div class="row">
                  
                    <div class="col-md-12">
                        <div class="p-4">
                            <div class="auth-logo text-center mb-4">
                                <img src="{{asset('images/logo.png')}}" alt="">
                            </div>
                            <h1 class="mb-3 text-18" style="color: #ffffff!important;">Reset Password</h1>
                           <form method="POST" action="{{ route('passwordResetuser',[$user->password_reset_token, $user->email]) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label" style="color: #ffffff!important;">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label" style="color: #ffffff!important;">{{ __('Confirm Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="current-password">

                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @if(Session::has('message') && Session::has('message') !='')
                            <div class="alert alert-card alert-success" role="alert">
                                <strong class="text-capitalize">Success!</strong> {{ Session::get('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        @endif
                        <div class="form-group row mb-0">
                          
                            <div class="col-md-8">
                                <button class="btn btn-primary btn-block btn-rounded mt-3">Reset Password</button>
                            </div>
                        </div>
                    </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('js/vendor/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/vendor/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/es5/script.min.js')}}"></script>
</body>

</html>