<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function postDetails(){
    	return $this->hasMany('App\PostDetail','post_id','id');
    }
}
