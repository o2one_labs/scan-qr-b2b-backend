@include('partials.head')
 
<body class="text-left">
    <div class='loadscreenProgress' id="processing" style="display:none">

        <div class="spinner spinner-primary mr-3" style="top: 50%"></div>



        </div>
    <!-- Pre Loader Strat  -->
    <!-- <div class='loadscreen' id="preloader">

        <div class="loader spinner-bubble spinner-bubble-primary">



        </div>
    </div> -->
    <!-- Pre Loader end  -->
    <div class="app-admin-wrap layout-sidebar-large clearfix">
@include('partials.header')


@include('partials.sidebar')
        <!--=============== Left side End ================-->

        <!-- ============ Body content start ============= -->
        <div class="main-content-wrap d-flex flex-column">
@yield('content')

            <!-- Footer Start -->
@include('partials.footer')
            <!-- fotter end -->
        </div>
        <!-- ============ Body content End ============= -->
    </div>
    <!--=============== End app-admin-wrap ================-->

    <!-- ============ Search UI Start ============= -->
{{-- @include('partials.search') --}}
    <!-- ============ Search UI End ============= -->

    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.3.1.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

    <script src="{{asset('js/vendor/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/vendor/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('js/vendor/toastr.min.js')}}"></script>
    <script src="{{asset('js/vendor/echarts.min.js')}}"></script>

    <script src="{{asset('js/es5/echart.options.min.js')}}"></script>
    <script src="{{asset('js/es5/dashboard.v1.script.min.js')}}"></script>

    <script src="{{asset('js/vendor/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/es5/script.min.js')}}"></script>
    <script src="{{asset('js/es5/sidebar.large.script.min.js')}}"></script>
    <script src="{{asset('js/form.validation.script.js')}}"></script>
    <script src="{{asset('js/datatables.script.js')}}"></script>
    <script src="{{asset('js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('js/chosen.jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vendor/pickadate/picker.js')}}"></script>
    <script src="{{asset('js/vendor/pickadate/picker.date.js')}}"></script>
    <script src="{{asset('js/vendor/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/fetch.js')}}"></script>
    <script src="{{asset('js/tmpl.min.js')}}"></script>
    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
{{--    <script src="{{asset('js/vendor/bootstrap-datetimepicker.min.js.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>--}}
    <script src="{{asset('js/prism.js')}}" type="text/javascript" charset="utf-8" ></script>
    <script src="{{asset('js/init.js')}}" type="text/javascript" charset="utf-8"></script>
            <script src="https://js.pusher.com/5.1/pusher.min.js"></script>

    @yield('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    

</body>

</html>