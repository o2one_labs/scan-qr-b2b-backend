@extends('layouts.apphome')
@section('content')
    <link rel="stylesheet" href="{{asset('selectpicker/css/bootstrap-select.min.css')}}">
    <style>
        .parsley-required {
            color: red;
        }
        #parsley-id-31 {
            position: inherit !important;
            top: 30px !important;
        }
        #parsley-id-43 {
            position: inherit !important;
            top: 30px !important;
        }
        #parsley-id-51 {
            position: inherit !important;
            top: 30px !important;
        }
        .btn-danger
        {
            padding: 10px 40px 8px 40px!important;
        }
        .btn_upload {
            cursor: pointer;
            display: inline-block;
            overflow: hidden;
            position: relative;
            color: #fff;
            background-color: #2a72d4;
            border: 1px solid #166b8a;
            padding: 5px 10px;
        }
        .btn_upload:hover,
        .btn_upload:focus {
            background-color: #7ca9e6;
        }
        .yes {
            display: flex;
            align-items: flex-start;
            margin-top: 10px !important;
        }
        .btn_upload input {
            cursor: pointer;
            height: 100%;
            position: absolute;
            filter: alpha(opacity=1);
            -moz-opacity: 0;
            opacity: 0;
        }
        .it {
            height: 100px;
            margin-left: 10px;
        }
        .btn-rmv1,
        .btn-rmv2,
        .btn-rmv3,
        .btn-rmv4,
        .btn-rmv5,
        .btn-rmv6,
        .btn-rmv7,
        .btn-rmv8,
        .btn-rmv9{
            display: none;
        }
        .rmv {
            color: #fff;
            border-radius: 30px;
            border: 1px solid #fff;
            display: inline-block;
            background: rgba(255, 0, 0, 1);
            margin: 2px -10px!important;
            position:absolute;
            padding: 0!important;
            height: 25px!important;
            width: 25px!important;
        }
        .btn.btn-primary.import_button_label
        {
            margin-top:0px!important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <a class="back_arrow" href="javascript: history.go(-1)">
                            <img src="{{asset('image/left-arrow.png')}}">
                        </a> Edit User
                    </div>
                    <form autocomplete="off" class="needs-validation" id="add_role_form" novalidate method="POST" action="{{route('user.edit', $user_details->id)}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationTooltip01">User Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="validationTooltip01" placeholder="User Name" name="name_en" value="{{$user_details->name_en}}" data-required="true" data-parsley-required-message="User Name is required" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationTooltip01 arabic_label" style="float: right;">اسم المستخدم<span style="color: red;">*</span></label>
                                <input type="text" class="form-control text-right" id="validationTooltip01" placeholder="اسم المستخدم" name="name_ar" value="{{$user_details->name_ar}}" data-required="true" data-parsley-required-message="اسم المستخدم مطلوب" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationTooltip01">Email <span style="color: red;">*</span></label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror " id="email-input" placeholder="Email" name="email" value="{{$user_details->email}}" data-required="true" data-parsley-required-message="Email is required" required>
                                @error('email')
                                <p style="color:red;">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="numbers-Only">Contact Number <span style="color: red;">*</span></label>
                                <input type="number" class="form-control  @error('mobile_number') is-invalid @enderror " id="numbers-Only" placeholder="Contact Number" name="mobile_number" value="{{$user_details->mobile_number}}" data-required="true" data-parsley-required-message="Contact Number is required" required>
                                <p id="error_mobile_message" style="color:red;"></p>
                                @error('mobile_number')
                                <p style="color:red;">{{$message}}</p>
                                @enderror
                            </div>
                        </div>

                        {{--<div class="form-row">
                            <label>User Address</label>
                            <div class="col-md-12 no-padding">
                                @if(isset($user_details->address[0]))
                                    @foreach($user_details->address as $user_addresses)
                                        @if($user_addresses->is_active == 1 && $user_addresses->is_delete == 0)
                                            <div class="col-md-6" style="margin-top: 17px;">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="card-title">
                                                            <h4>{{$user_addresses->type->name_en}}</h4>
                                                            <input type="hidden" name="address_id[{{$user_addresses->id}}]" value="{{$user_addresses->id}}">
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-4 mb-3">
                                                                <label for="validationTooltip01">First Name</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="first_name" placeholder="Area" name="first_name[{{$user_addresses->id}}]" value="{{$user_addresses->first_name}}" data-required="true" data-parsley-required-message="Area is required" disabled>
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <label for="validationTooltip01">Last Name</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="last_name" placeholder="Block" name="last_name[{{$user_addresses->id}}]" value="{{$user_addresses->last_name}}" data-required="true" data-parsley-required-message="Block is required" disabled>
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <label for="validationTooltip01">Mobile Number</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="mobile_number" placeholder="Avenue" name="mobile_number[{{$user_addresses->id}}]" value="{{$user_addresses->mobile_number}}" data-required="true" data-parsley-required-message="Avenue is required" disabled>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-4 mb-3">
                                                                <label for="validationTooltip01">Area</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="area" placeholder="Area" name="area[{{$user_addresses->id}}]" value="{{$user_addresses->area}}" data-required="true" data-parsley-required-message="Area is required" disabled>
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <label for="validationTooltip01">Block</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="block" placeholder="Block" name="block_no[{{$user_addresses->id}}]" value="{{$user_addresses->block_no}}" data-required="true" data-parsley-required-message="Block is required" disabled>
                                                            </div>
                                                            <div class="col-md-4 mb-3">
                                                                <label for="validationTooltip01">Avenue</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="street" placeholder="Avenue" name="avenue[{{$user_addresses->id}}]" value="{{$user_addresses->avenue}}" data-required="true" data-parsley-required-message="Avenue is required" disabled>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-6 mb-3">
                                                                <label for="validationTooltip01">Street</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="street" placeholder="Street" name="street[{{$user_addresses->id}}]" value="{{$user_addresses->street}}" data-required="true" data-parsley-required-message="Street is required" disabled>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <label for="validationTooltip01">Building Number/Name</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="building" placeholder="Building Number/Name" name="building[{{$user_addresses->id}}]" value="{{$user_addresses->building}}" data-required="true" data-parsley-required-message="Building Number/Name is required" disabled>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="col-md-6 mb-3">
                                                                <label for="validationTooltip01">Floor</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="floor" placeholder="Floor" name="floor[{{$user_addresses->id}}]" value="{{$user_addresses->floor}}" data-required="true" data-parsley-required-message="Floor is required" disabled>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <label for="validationTooltip01">Apartment</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="apartment" placeholder="Apartment" name="apartment_no[{{$user_addresses->id}}]" value="{{$user_addresses->apartment_no}}" data-required="true" data-parsley-required-message="Apartment is required" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 mb-3">
                                                                <label for="validationTooltip01">Additional Info</label>
                                                                <input type="text" autocomplete="off" class="form-control" id="floor" placeholder="Floor" name="floor[{{$user_addresses->id}}]" value="{{$user_addresses->additional_info ? $user_addresses->additional_info : "--"}}" data-required="true" data-parsley-required-message="Floor is required" disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @else
                                    No Record Found
                                @endif
                            </div>
                        </div>--}}

                        <button type="button" id="add_role" value="Update" class="btn btn-primary btn-gradient-info mr-2 import_button_label">Update</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/parsley.js')}}"></script>
    <script src="{{ asset('js/addResvalidation.js')}}"></script>
    <script src="{{asset('selectpicker/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/vendor/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.js')}}"></script>
    <script type="text/javascript">
        // form submit
        $("#add_role").click(function(event) {
            $('.bs-placeholder').css('margin-top','-21px');
            if($("#add_role_form").parsley().validate()){
                // $(this).attr("disabled",true);
                $('#add_role_form').submit();
            }
        });

        function validateEmailField(){
            return new Promise((resolve, reject) => {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                let isFormValidated = false;
                $("#email-input").value = $("#email-input").val();
                let val = $("#email-input").val();
                var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                let flag = regex.test(val);
                /*if(!flag){
                    $("#error_email_message").text("Required Valid Email");
                    $("#error_email_message").show();
                    isFormValidated = false;
                    resolve(isFormValidated)
                }else{*/
                var email = $("#email-input").val();
                //console.log(email); return false;
                $.ajax({

                    url: "{{route('Restaurent.subAdminrole.email')}}",
                    method: 'POST',
                    data:{email:email},
                    type:"json",
                    success:function(response)
                    {
                        console.log(email);
                        if(response.status ==1)
                        {
                            $("#error_email_message").hide();
                            isFormValidated = true;
                            resolve(isFormValidated)
                        }
                        else
                        {
                            $("#error_email_message").text(response.msg);
                            $("#error_email_message").show();
                            isFormValidated = false;
                            resolve(isFormValidated)
                        }
                    },
                });
                //


                // }


            })
        }
    </script>
@endsection