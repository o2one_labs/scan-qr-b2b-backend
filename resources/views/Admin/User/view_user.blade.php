@extends('layouts.apphome')
@section('content')
    <link rel="stylesheet" href="{{asset('selectpicker/css/bootstrap-select.min.css')}}">
    <style>  
        .mb-0 > a {
            display: inline-block;
            width: 100%;
            padding:0.75rem 1.25rem;
        }
        .card-header {
            padding:0;
        }
        .parsley-required {
            color: red;
        }
        #parsley-id-31 {
            position: inherit !important;
            top: 30px !important;
        }
        #parsley-id-43 {
            position: inherit !important;
            top: 30px !important;
        }
        #parsley-id-51 {
            position: inherit !important;
            top: 30px !important;
        }
        .btn-danger
        {
            padding: 10px 40px 8px 40px!important;
        }
        .btn_upload {
            cursor: pointer;
            display: inline-block;
            overflow: hidden;
            position: relative;
            color: #fff;
            background-color: #2a72d4;
            border: 1px solid #166b8a;
            padding: 5px 10px;
        }
        .btn_upload:hover,
        .btn_upload:focus {
            background-color: #7ca9e6;
        }
        .yes {
            display: flex;
            align-items: flex-start;
            margin-top: 10px !important;
        }
        .btn_upload input {
            cursor: pointer;
            height: 100%;
            position: absolute;
            filter: alpha(opacity=1);
            -moz-opacity: 0;
            opacity: 0;
        }
        .it {
            height: 100px;
            margin-left: 10px;
        }
        .btn-rmv1,
        .btn-rmv2,
        .btn-rmv3,
        .btn-rmv4,
        .btn-rmv5,
        .btn-rmv6,
        .btn-rmv7,
        .btn-rmv8,
        .btn-rmv9{
            display: none;
        }
        .rmv {
            color: #fff;
            border-radius: 30px;
            border: 1px solid #fff;
            display: inline-block;
            background: rgba(255, 0, 0, 1);
            margin: 2px -10px!important;
            position:absolute;
            padding: 0!important;
            height: 25px!important;
            width: 25px!important;
        }
        .btn.btn-primary.import_button_label
        {
            margin-top:0px!important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <a class="back_arrow" href="javascript: history.go(-1)">
                            <img src="{{asset('image/left-arrow.png')}}">
                        </a> View User
                    </div>

                    <div class="accordion" id="accordionRightIcon">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0">
                                    <a data-toggle="collapse" class="text-default collapsed" href="#accordion-item-icons-1" aria-expanded="false" style="color: #d24c7c;">
                                        User Details</a>
                                </h6>

                            </div>

                            <div id="accordion-item-icons-1" class="collapse " data-parent="#accordionRightIcon" style="">
                                <div class="card-body">
                                    <div class="separator-breadcrumb border-top"></div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationTooltip01">User Name <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" id="validationTooltip01" placeholder="User Name" name="name_en" value="{{!empty($user_details->name_en) ? $user_details->name_en : ''}}" data-required="true" data-parsley-required-message="User Name is required" disabled>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationTooltip01 arabic_label" style="float: right;">اسم المستخدم<span style="color: red;">*</span></label>
                                            <input type="text" class="form-control text-right" id="validationTooltip01" placeholder="اسم المستخدم" name="name_ar" value="{{!empty($user_details->name_ar)}}" data-required="true" data-parsley-required-message="اسم المستخدم مطلوب" disabled>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationTooltip01">Email <span style="color: red;">*</span></label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror " id="email-input"  oninput='validateEmailField()' placeholder="Email" name="email" value="{{$user_details->email}}" data-required="true" data-parsley-required-message="Email is required" disabled>
                                            @error('email')
                                            <p style="color:red;">{{$message}}</p>
                                            @enderror
                                            <p id="error_email_message" style="color:red;"></p>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="numbers-Only">Contact Number <span style="color: red;">*</span></label>
                                            <input type="number" class="form-control" id="numbers-Only"  oninput="validatePhoneNumber()" placeholder="Contact Number" name="mobile_number" value="{{$user_details->mobile_number}}" data-required="true" data-parsley-required-message="Contact Number is required" disabled>
                                            <p id="error_mobile_message" style="color:red;"></p>
                                            @error('m_mobile_no')
                                            <p style="color:red;">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationTooltip01">Date of Birth <span style="color: red;">*</span></label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror " id="email-input"  oninput='validateEmailField()' placeholder="Email" name="email" value="{{$user_details->date_of_birth ? $user_details->date_of_birth : 'NA'}}" data-required="true" data-parsley-required-message="Email is required" disabled>
                                            @error('email')
                                            <p style="color:red;">{{$message}}</p>
                                            @enderror
                                            <p id="error_email_message" style="color:red;"></p>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="numbers-Only">Gender <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" id="numbers-Only"  oninput="validatePhoneNumber()" placeholder="Contact Number" name="mobile_number" value="{{$user_details->gender == 1 ? 'Male' : ($user_details->gender == 2 ? 'Female' : ($user_details->gender == 3 ? 'Transgender' : 'NA'))}}" data-required="true" data-parsley-required-message="Contact Number is required" disabled>
                                            <p id="error_mobile_message" style="color:red;"></p>
                                            @error('m_mobile_no')
                                            <p style="color:red;">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        {{--<div class="col-md-6 mb-3">
                                            <label for="validationTooltip01">OS <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" id="email-input" placeholder="OS" name="os" value="{{$user_details->device ? ($user_details->device->device_type == 'android' ? 'Android' : ($user_details->device->device_type == 'ios' ? 'iOS' : '--')) : '--'}}" data-required="true" data-parsley-required-message="Email is required" disabled>
                                        </div>--}}
                                        <div class="col-md-6 mb-3">
                                            <label for="numbers-Only">Status <span style="color: red;">*</span></label>
                                            <input type="text" class="form-control" id="numbers-Only" placeholder="Status" name="status" value="{{$user_details->is_active == 1 ? 'Unblocked' : 'Blocked'}}" data-required="true" data-parsley-required-message="Contact Number is required" disabled>
                                            <p id="error_mobile_message" style="color:red;"></p>
                                            @error('m_mobile_no')
                                            <p style="color:red;">{{$message}}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <label>User Address</label>
                                        <div class="col-md-12 no-padding">
                                            @if(isset($user_details->address[0]))
                                                @foreach($user_details->address as $user_addresses)
                                                    @if($user_addresses->is_active == 1 && $user_addresses->is_delete == 0)
                                                        <div class="col-md-6" style="margin-top: 17px;">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="card-title">
                                                                        <h4>{{!empty($user_addresses->type) ? $user_addresses->type->name_en : ''}}</h4>
                                                                        <input type="hidden" name="address_id[{{$user_addresses->id}}]" value="{{$user_addresses->id}}">
                                                                    </div>

                                                                    <div class="form-row">
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">User Name</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="first_name" placeholder="Area" name="first_name[{{$user_addresses->id}}]" value="{{$user_addresses->first_name}}" data-required="true" data-parsley-required-message="Area is required" disabled>
                                                                        </div>
                                                                        {{--<div class="col-md-4 mb-3">
                                                                            <label for="validationTooltip01">Last Name</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="last_name" placeholder="Block" name="last_name[{{$user_addresses->id}}]" value="{{$user_addresses->last_name}}" data-required="true" data-parsley-required-message="Block is required" disabled>
                                                                        </div>--}}
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Mobile Number</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="mobile_number" placeholder="Avenue" name="mobile_number[{{$user_addresses->id}}]" value="{{$user_addresses->mobile_number}}" data-required="true" data-parsley-required-message="Avenue is required" disabled>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-row">
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Area</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="area" placeholder="Area" name="area[{{$user_addresses->id}}]" value="{{$user_addresses->area}}" data-required="true" data-parsley-required-message="Area is required" disabled>
                                                                        </div>
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Block</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="block" placeholder="Block" name="block_no[{{$user_addresses->id}}]" value="{{$user_addresses->block_no}}" data-required="true" data-parsley-required-message="Block is required" disabled>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Avenue</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="street" placeholder="Avenue" name="avenue[{{$user_addresses->id}}]" value="{{$user_addresses->avenue}}" data-required="true" data-parsley-required-message="Avenue is required" disabled>
                                                                        </div>
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Street</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="street" placeholder="Street" name="street[{{$user_addresses->id}}]" value="{{$user_addresses->street}}" data-required="true" data-parsley-required-message="Street is required" disabled>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-row">
                                                                        
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Building Number/Name</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="building" placeholder="Building Number/Name" name="building[{{$user_addresses->id}}]" value="{{$user_addresses->building}}" data-required="true" data-parsley-required-message="Building Number/Name is required" disabled>
                                                                        </div>
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Nickname</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="building" placeholder="Building Number/Name" name="building[{{$user_addresses->id}}]" value="{{$user_addresses->address_nickname}}" data-required="true" data-parsley-required-message="Building Number/Name is required" disabled>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-row">
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Floor</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="floor" placeholder="Floor" name="floor[{{$user_addresses->id}}]" value="{{$user_addresses->floor}}" data-required="true" data-parsley-required-message="Floor is required" disabled>
                                                                        </div>
                                                                        <div class="col-md-6 mb-3">
                                                                            <label for="validationTooltip01">Home</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="apartment" placeholder="Apartment" name="apartment_no[{{$user_addresses->id}}]" value="{{$user_addresses->apartment_no}}" data-required="true" data-parsley-required-message="Apartment is required" disabled>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="col-md-12 mb-3">
                                                                            <label for="validationTooltip01">Additional Info</label>
                                                                            <input type="text" autocomplete="off" class="form-control" id="floor" placeholder="Floor" name="floor[{{$user_addresses->id}}]" value="{{$user_addresses->additional_info}}" data-required="true" data-parsley-required-message="Floor is required" disabled>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                No Record Found
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0">
                                    <a data-toggle="collapse" class="text-default collapsed"
                                       href="#accordion-item-icons-2" style="color: #d24c7c;">
                                        Wallet History</a>
                                </h6>
                            </div>



                            <div id="accordion-item-icons-2" class="collapse " data-parent="#accordionRightIcon">
                                <div class="card-body">
                                    <div class="col-md-4 pull-right">
                                        <h4 class="pull-right">Balance Amount : {{"KD ".number_format($user_details->wallet,3)}} <a type="button"class="update_amount btn btn-primary" id="{{$id}}" title="Update Amount" style="height: 28px;"><span class="glyphicon glyphicon-edit"></span> </a></h4>
                                    </div>
                                    <div class="table-responsive" style="width: 100%;">
                                        <table id="user_table" class="table table-bordered table-striped display nowrap">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Amount</th>
                                                <th>Type</th>
                                                <th>Transaction ID</th>
                                                <th>Order ID</th>
                                                <th>Ordered From</th>
                                                <th>Order Time</th>
                                                <th>Outstanding Amount</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/parsley.js')}}"></script>
    <script src="{{ asset('js/addResvalidation.js')}}"></script>
    <script src="{{asset('selectpicker/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/vendor/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.js')}}"></script>
    <script src="{{asset('js/dataTables.checkboxes.min.js')}}"></script>
    <script src="{{asset('js/dataTables.checkboxes.js')}}"></script>
    <script type="text/javascript">
        // form submit
        $("#add_role").click(function(event) {
            $('.bs-placeholder').css('margin-top','-21px');
            if($("#add_role_form").parsley().validate()){
                // $(this).attr("disabled",true);
                $('#add_role_form').submit();
            }
        });
        $(document).ready(function(){
            $('#user_table').on( 'init.dt', function () {
                $("#user_table thead tr th:first").removeClass('sorting_asc');
            } ).DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: {
                    url: "{{ route('user.view',$id) }}",
                },

                columns: [
                    {
                        data: 'check',
                        name: 'check',
                        orderable: false,
                        "width":"1%",
                    },
                    {
                        data: 'amount',
                        name: 'amount',
                        orderable: true,
                        width : "20%",
                    },
                    {
                        data: 'type',
                        name: 'type',
                        orderable: false,
                        width : "20%",
                    },
                    {
                        data: 'TranID',
                        name: 'TranID',
                        orderable: false,
                        width : "20%",
                    },
                    {
                        data: 'order_id',
                        name: 'order_id',
                        orderable: false,
                        width : "20%",
                    },
                    {
                        data: 'order_from',
                        name: 'order_from',
                        orderable: false,
                        width : "20%",
                    },
                    {
                        data: 'order_time',
                        name: 'order_time',
                        orderable: false,
                        width : "20%",
                    },
                    {
                        data: 'last_amount',
                        name: 'last_amount',
                        orderable: false,
                        width : "20%",
                    }
                ]
            });
        });

        function validateEmailField(){
            return new Promise((resolve, reject) => {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                let isFormValidated = false;
                $("#email-input").value = $("#email-input").val();
                let val = $("#email-input").val();
                var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                let flag = regex.test(val);
                /*if(!flag){
                    $("#error_email_message").text("Required Valid Email");
                    $("#error_email_message").show();
                    isFormValidated = false;
                    resolve(isFormValidated)
                }else{*/
                var email = $("#email-input").val();
                //console.log(email); return false;
                $.ajax({

                    url: "{{route('Restaurent.subAdminrole.email')}}",
                    method: 'POST',
                    data:{email:email},
                    type:"json",
                    success:function(response)
                    {
                        console.log(email);
                        if(response.status ==1)
                        {
                            $("#error_email_message").hide();
                            isFormValidated = true;
                            resolve(isFormValidated)
                        }
                        else
                        {
                            $("#error_email_message").text(response.msg);
                            $("#error_email_message").show();
                            isFormValidated = false;
                            resolve(isFormValidated)
                        }
                    },
                });
                //


                // }


            })
        }
        $(document).on('click', '.update_amount', function(){
        user_id = $(this).attr('id');
        type=1;
        swal({
            title: 'Are you sure?',
            text: "You want to Update wallet Amount",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3f51b5',
            cancelButtonColor: '#ff4081',
            confirmButtonText: 'OK ',
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "btn btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "OK",
                    value: true,
                    visible: true,
                    className: "btn btn-info",
                    closeModal: true,
                }
            }

        }).then(function(isConfirm){
            if(isConfirm){
                //Input Amount
                swal({
                    title: 'Enter wallet Amount',
                    html: '<div class="form-row">\n' +
                        '                        <div class="col-md-12 mb-2">\n' +
                        '                            <label class="pull-left">Wallet Amount(KD)</label><input type="number" step="0.001" min="0" id="wallet_amount" class="form-control" placeholder="Wallet Amount" name="wallet_amount" required>\n' +
                        '                        </div>\n' +
                        '                    </div>',
                    showConfirmButton: true,
                    customClass: 'swal2-overflow',
                    onOpen: function() {
                        swal.disableConfirmButton();
                        $('#wallet_amount').on('input', function (e) {
                            x=$('#wallet_amount').val();
                            
                            if (isNaN(x) || x < 1 || x > 10) {} else {
                                swal.enableConfirmButton();
                            }
                        })
                    }
                }).then(function(result) {
                    myArrayOfThings = [
                        { id: 5, name: 'credit' },
                        { id: 6, name: 'debit' }
                    ];


                    var options = {};
                    $.map(myArrayOfThings,
                        function(o) {
                            options[o.id] = o.name;
                        });
                    swal({
                        title: "Select Status",
                        input: "select",
                        class: "sstatus",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        inputOptions: options,
                        inputPlaceholder: "Select Status",
                        onOpen: function() {
                            swal.disableConfirmButton();
                            $('.swal2-select').on('change', function (e) {
                                x=$('.swal2-select').val();
                                console.log(x);
                                if (x == '') {} else {
                                    swal.enableConfirmButton();
                                }
                            })
                        }
                    }).then(function (inputValue) {
                        // alert(type);
                        // alert(inputValue);
                        // alert($('#wallet_amount').val())
                        // alert(user_id);
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url:"{{route('user.wallet_amount_update')}}",
                            method: 'POST',
                            data:{user_id:user_id,type:type,amount_type:inputValue,amount:$('#wallet_amount').val()},
                            type:"json",
                            success: function(data) {
                                if(data.status ==1)
                                {
                                    $('#user_table').DataTable().ajax.reload();
                                swal("Success!", "Wallet Amount updated successfully !!!", "success");
                                }else{
                                     swal("Error!", data.message, "error");   
                                }
                            },
                            error:function(){
                                swal({
                                    text: 'Something went wrong',
                                    button: {
                                        text: "OK",
                                        value: true,
                                        visible: true,
                                        className: "btn btn-primary"
                                    }
                                })
                            }
                        });
                    });
                });
                //End

            }
        })

    });
    </script>
@endsection