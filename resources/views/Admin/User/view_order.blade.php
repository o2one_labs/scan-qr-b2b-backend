@extends('layouts.apphome')
@section('content')
    <link rel="stylesheet" href="{{asset('selectpicker/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/timeline_style.css')}}">
    <style>
        .mb-0 > a {
            display: inline-block;
            width: 100%;
            padding:0.75rem 1.25rem;
        }
        .card-header {
            padding:0;
        }
        .parsley-required {
            color: red;
        }
        #parsley-id-31 {
            position: inherit !important;
            top: 30px !important;
        }
        #parsley-id-43 {
            position: inherit !important;
            top: 30px !important;
        }
        #parsley-id-51 {
            position: inherit !important;
            top: 30px !important;
        }
        .btn-danger
        {
            padding: 10px 40px 8px 40px!important;
        }
        .btn_upload {
            cursor: pointer;
            display: inline-block;
            overflow: hidden;
            position: relative;
            color: #fff;
            background-color: #2a72d4;
            border: 1px solid #166b8a;
            padding: 5px 10px;
        }
        .btn_upload:hover,
        .btn_upload:focus {
            background-color: #7ca9e6;
        }
        .yes {
            display: flex;
            align-items: flex-start;
            margin-top: 10px !important;
        }
        .btn_upload input {
            cursor: pointer;
            height: 100%;
            position: absolute;
            filter: alpha(opacity=1);
            -moz-opacity: 0;
            opacity: 0;
        }
        .it {
            height: 100px;
            margin-left: 10px;
        }
        .btn-rmv1,
        .btn-rmv2,
        .btn-rmv3,
        .btn-rmv4,
        .btn-rmv5,
        .btn-rmv6,
        .btn-rmv7,
        .btn-rmv8,
        .btn-rmv9{
            display: none;
        }
        .rmv {
            color: #fff;
            border-radius: 30px;
            border: 1px solid #fff;
            display: inline-block;
            background: rgba(255, 0, 0, 1);
            margin: 2px -10px!important;
            position:absolute;
            padding: 0!important;
            height: 25px!important;
            width: 25px!important;
        }
        .btn.btn-primary.import_button_label
        {
            margin-top:0px!important;
        }

        .table td, .table th {
            border-top: transparent !important;
        }
    </style>
    @php $total_amount = 0; @endphp
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <a class="back_arrow" href="javascript: history.go(-1)">
                            <img src="{{asset('image/left-arrow.png')}}">
                        </a> Details For Order #{{$order_details->order_id}}
                    </div>

                    <div class="col-md-6" style="margin-top: 17px;">
                        <div class="card" style="background-color: #d96198bf;height: 356px;">
                            <div class="card-body">
                                <div class="card-title">
                                    <h4>Pickup Details</h4><hr style="border-top: 1px solid black !important;">

                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Restaurant Name</th>
                                                    <td style="width: 60%;">{{$order_details->order_restaurant->restaurant->r_name_en}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Pickup Address</th>
                                                    <?php
                                                    // $loc = json_decode($order_details->order_restaurant->restaurant->location);
                                                    $loc = json_decode($order_details->branch_details->location);
                                                    ?>
                                                    <td style="width: 60%;">{{$loc->location}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Order Date</th>
                                                    <td style="width: 60%;">{{date("d-m-Y",strtotime($order_details->created_at))}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Order Time</th>
                                                    <td style="width: 60%;">{{date("H:i",strtotime($order_details->created_at))}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Order Status</th>
                                                    <td style="width: 60%;">{{$order_details->order_status == 1 ? 'Placed' : ($order_details->order_status == 2 ? 'Preparing' : ($order_details->order_status == 3 ? 'Picked' : ($order_details->order_status == 4 ? 'Delivering' : ($order_details->order_status == 5 ? 'Delivered' : ($order_details->order_status == 6 ? 'Rejected' : ($order_details->order_status == 7 ? 'Failed' : ($order_details->order_status == 8 ? 'Cancelled' : ($order_details->order_status == 0 ? 'Pending' : ($order_details->order_status == 9 ? 'Not Completed' : 'NA')))))))))}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Payment Mode</th>
                                                    <td style="width: 60%;">{{$order_details->payment_mode == 1 ? 'Wallet' : ($order_details->payment_mode == 2 ? 'Cash' : ($order_details->payment_mode == 3 ? 'Card' : ($order_details->payment_mode == 4 ? 'Wallet & Cash' : ($order_details->payment_mode == 5 ? 'Wallet & Card' : ($order_details->payment_mode == 6 ? 'KNET': ($order_details->payment_mode == 7 ? 'Wallet & KNET' : 'NA'))))))}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Payment Status</th>
                                                    <td style="width: 60%;">
                                                        @if($order_details->order_status == 6 ||$order_details->order_status == 7 ||$order_details->order_status == 8)
                                                        Refunded
                                                        @elseif($order_details->order_status == 0)
                                                        {{'Failed'}}
                                                        @else
                                                        {{$order_details->payment_mode == 2 || $order_details->payment_mode == 4     ? ($order_details->paymant_status == 0 ? "Pending" : ($order_details->paymant_status == 1 ? "Success" : "Failed")) : ($order_details->paymant_status == 1 ? "Success" :($order_details->paymant_status == 0 ? "Pending" : "Failed"))}}
                                                        @endif
                                                        </td>
                                                </tr>
                                                
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-top: 17px;">
                        <div class="card" style="background-color: #d96198bf;height: 356px;">
                            <div class="card-body">
                                <div class="card-title">
                                    <h4>Delivery Details</h4><hr style="border-top: 1px solid black !important;">

                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;vertical-align: top !important;">Delivery Details</th>
                                                    <?php $a = "<strong>";?>
                                    <?php $b = "</strong>";?>
                                    <td>{{$order_details->user_address->first_name}}<br/>
                                        +965{{-$order_details->user_address->mobile_number}} <br/>

                            <!-- AREA -->
                                        @if($order_details->user_address->area != "")
                                            <strong>Area</strong> : {{$order_details->user_address->area}}<br/>
                                        @endif
                                        


                            <!-- Block -->
                                        @if($order_details->user_address->block_no != "")
                                            <strong>Block No</strong> : {{$order_details->user_address->block_no}}<br/>
                                        @endif
                                        
                            <!-- Avenue-->
                                        @if($order_details->user_address->avenue != "")
                                            <strong>Avenue</strong> : {{$order_details->user_address->avenue}}<br/>
                                        @endif
                                        

                            <!-- Street-->
                                            
                                        @if($order_details->user_address->street != "")
                                            <strong>Street</strong> : {{$order_details->user_address->street}}
                                            <br/>
                                        @endif
                                        

                                        @if($order_details->user_address->address_type != 3)
                                <!-- Building No.-->
                                            @if($order_details->user_address->building != "")
                                                <strong>Building No</strong> : {{$order_details->user_address->building}}
                                                <br/>
                                            @endif
                                            
                                <!-- Floor No.-->

                                            @if($order_details->user_address->floor != "")
                                                <strong>Floor</strong> : {{$order_details->user_address->floor}}
                                            <br/>
                                            @endif

                                        @endif
                            <!-- Apartment No.-->

                                            @if($order_details->user_address->apartment_no != "")
                                                @if($order_details->user_address->address_type == 1)
                                                <strong>Apartment No</strong> : 
                                                @elseif($order_details->user_address->address_type == 2)<!--Office-->
                                                <strong>Office No</strong> : 
                                                @elseif($order_details->user_address->address_type == 3)
                                                <strong>House</strong> : 
                                                @endif
                                                {{$order_details->user_address->apartment_no}}
                                                <br/>
                                            @endif
                                            
                                                </td>
                                                </tr>

                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Delivery Date</th>
                                                    <td style="width: 60%;">
                                                        @if($order_details->order_status == 5)
                                                        {{date("d-m-Y",strtotime($order_details->updated_at))}}
                                                        @else
                                                        {{'--'}}
                                                        @endif
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Delivery Time</th>
                                                    <td style="width: 60%;">{{($order_details->order_status == 5) ? date("H:i",strtotime($order_details->updated_at)) : '--'}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Target Delivery Time</th>
                                                    <?php
                                                    $res_lat = json_decode($order_details->branch_details->location)->latitude; 
                                                    $res_long = json_decode($order_details->branch_details->location)->longitude; 
                                                    $time = explode("-",get_delivery_time($res_lat, $res_long, $order_details->user_address->lat, $order_details->user_address->long, $area));
                                                    $selectedTime = $order_details->created_at;
                                                    $endTime = strtotime($time[1], strtotime($selectedTime));
                                                    // echo date('h:i:s', $endTime);
                                                    ?>
                                                    <!-- <td style="width: 60%;">{{$time[1]}}</td> -->
                                                    <td style="width: 60%;">{{date('h:i:s A', $endTime)}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 40%;font-size: 14px;">Special Request</th>
                                                    <td style="width: 60%;">{{$order_details->special_request ? $order_details->special_request : "--"}}</td>
                                                </tr>
                                                
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 17px;">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title">
                                    <h4>Tracking Details</h4><hr style="border-top: 1px solid black !important;">

                                    <ul class="timeline" id="timeline">
                                        <li class="li {{$complete1}}">
                                            <div class="timestamp" @if($complete1 != 'complete') style="padding: 20px;" @endif>
                                                <span class="author">{{$created_at1}}</span>
                                                <span class="date">{{$created_at1 ? '0 Mins' : ''}}</span>
                                            </div>
                                            <div class="status">
                                                <h4> Order Submitted </h4>
                                            </div>
                                        </li>
                                        @if($is_rejected)
                                            <li class="li {{$complete6}}">
                                                <div class="timestamp" @if($complete6 != 'complete') style="padding: 20px;" @endif>
                                                    <span class="author">{{$created_at6}}</span>
                                                    <span class="date">{{$timediff5 ? $timediff5->format('%h Hrs %i Mins') : ''}}</span>
                                                </div>
                                                <div class="status">
                                                    <h4> Rejected </h4>
                                                </div>
                                            </li>
                                        @else
                                            <li class="li {{$complete2}}">
                                                <div class="timestamp" @if($complete2 != 'complete') style="padding: 20px;" @endif>
                                                    <span class="author">{{$created_at2}}</span>
                                                    <span class="date">{{$timediff1 ? $timediff1->format('%h Hrs %i Mins') : ''}}</span>
                                                </div>
                                                <div class="status">
                                                    <h4> Being Prepared </h4>
                                                </div>
                                            </li>
                                            <li class="li {{$complete3}}">
                                                <div class="timestamp" @if($complete3 != 'complete') style="padding: 20px;" @endif>
                                                    <span class="author">{{$created_at3}}</span>
                                                    <span class="date">{{$timediff2 ? $timediff2->format('%h Hrs %i Mins') : ''}}</span>
                                                </div>
                                                <div class="status">
                                                    <h4> Order Picked </h4>
                                                </div>
                                            </li>
                                            <li class="li {{$complete4}}">
                                                <div class="timestamp" @if($complete4 != 'complete') style="padding: 20px;" @endif>
                                                    <span class="author">{{$created_at4}}</span>
                                                    <span class="date">{{$timediff3 ? $timediff3->format('%h Hrs %i Mins') : ''}}</span>
                                                </div>
                                                <div class="status">
                                                    <h4> Delivering Now </h4>
                                                </div>
                                            </li>
                                            <li class="li {{$complete5}}">
                                                <div class="timestamp" @if($complete5 != 'complete') style="padding: 20px;" @endif>
                                                    <span class="author">{{$created_at5}}</span>
                                                    <span class="date">{{$timediff4 ? $timediff4->format('%h Hrs %i Mins') : ''}}</span>
                                                </div>
                                                <div class="status">
                                                    <h4> Delivered </h4>
                                                </div>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 17px;">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title">
                                    <h4>Invoice Details <span class="pull-right"> <a href="{{route('admin.order.print_invoice',$id)}}" class="print" style="color: #d2007d!important;cursor: pointer;"> Print </a> </span></h4><hr style="border-top: 1px solid black !important;">

                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 33%;font-size: 14px;">Item Name</th>
                                                    <th style="width: 33%;font-size: 14px;">Amount</th>
                                                    <th style="width: 33%;font-size: 14px;">Special Message</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($order_details->order_item as $item_details)
                                                    <tr>
                                                        <td>{{$item_details->item_name_en}} X {{$item_details->quantity}}
                                                            @if(isset($order_details->order_item_choices[0]))
                                                                @foreach($order_details->order_item_choices as $choice_details)
                                                                    @if($choice_details->item_id == $item_details->item_id)
                                                                        @foreach($choice_details->order_item_choices as $choice_details_rowset)
                                                                            @if($item_details->id == $choice_details_rowset->order_item_id)
                                                                            <br>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;<span>+ {{$choice_details_rowset->name_en}}</span>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </td>
                                                        <td>KD {{number_format($item_details->total_price,3)}}</td>
                                                        @php $total_amount = $total_amount + $item_details->total_price; @endphp
                                                        <td style="vertical-align: top !important;word-break: break-all;">{{$item_details->special_message ? $item_details->special_message : '--'}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <hr style="border-top: 1px solid black !important;">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 33%;font-size: 14px;">Item Total</th>
                                                    <td>KD {{number_format($total_amount,3)}}</td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 33%;font-size: 14px;">Delivery Charges</th>
                                                    <td>KD {{number_format($order_details->delivery_charge,3)}}</td>
                                                </tr>
                                                @if($order_details->order_coupons)
                                                    <tr>
                                                        <th style="width: 33%;font-size: 14px;">Coupon Applied {{isset($order_details->order_coupons) ? '(' . $order_details->order_coupons->name_en . ')' : '--'}}</th>
                                                        <td>KD {{number_format($order_details->coupon_amount,3)}}</td>
                                                    </tr>
                                                @endif
                                            </thead>
                                        </table>
                                        <hr style="border-top: 1px solid black !important;">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th style="width: 33%;font-size: 14px;">Total</th>
                                                <td><strong>KD {{number_format($order_details->overall_amount + $order_details->delivery_charge,3)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <th style="width: 33%;font-size: 14px;">Paid Via</th>
                                                <td>{{$order_details->payment_mode == 1 ? 'Wallet' : ($order_details->payment_mode == 2 ? 'Cash' : ($order_details->payment_mode == 3 ? 'Card' : ($order_details->payment_mode == 4 ? 'Wallet & Cash' : ($order_details->payment_mode == 5 ? 'Wallet & Card' : ($order_details->payment_mode == 6 ? 'KNET' : ($order_details->payment_mode == 7 ? 'Wallet & KNET' : 'NA'))))))}}</td>
                                            </tr>
                                            @if($order_details->payment_mode == 4)
                                                <tr>
                                                    <th style="width: 25%;font-size: 14px;">Paid Via Wallet</th>
                                                    <td style="width: 25%;">KD {{number_format($order_details->wallet_amount,3)}}</td>
                                                    <td style="width: 25%;"></td>
                                                    <td style="width: 25%;"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 25%;font-size: 14px;">Paid Via Cash</th>
                                                    <td style="width: 25%;">KD {{number_format($order_details->cash,3)}}</td>
                                                    <td style="width: 25%;"></td>
                                                    <td style="width: 25%;"></td>
                                                </tr>
                                            @endif
                                            @if($order_details->payment_mode == 5)
                                                <tr>
                                                    <th style="width: 25%;font-size: 14px;">Paid Via Wallet</th>
                                                    <td style="width: 25%;">KD {{number_format($order_details->wallet_amount,3)}}</td>
                                                    <td style="width: 25%;"></td>
                                                    <td style="width: 25%;"></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 25%;font-size: 14px;">Paid Via Card</th>
                                                    <td style="width: 25%;">KD {{number_format($order_details->card,3)}}</td>
                                                    <td style="width: 25%;"></td>
                                                    <td style="width: 25%;"></td>
                                                </tr>
                                            @endif
                                            @if($order_details->payment_mode == 3 || $order_details->payment_mode == 5 || $order_details->payment_mode == 6 || $order_details->payment_mode == 7)
                                                @if($order_details->cash != 0.000)
                                                    <tr>
                                                        <th style="width: 25%;font-size: 14px;">Paid Via Cash</th>
                                                        <td style="width: 25%;">KD {{number_format($order_details->cash,3)}}</td>
                                                        <td style="width: 25%;"></td>
                                                        <td style="width: 25%;"></td>
                                                    </tr>
                                                @endif
                                            @endif
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/parsley.js')}}"></script>
    <script src="{{ asset('js/addResvalidation.js')}}"></script>
    <script src="{{asset('selectpicker/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/vendor/sweetalert2.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.js')}}"></script>
@endsection