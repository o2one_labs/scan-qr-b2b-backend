
    //Validate Name
      function validateName()
      {
        let isFormValidated = false;
        let val = $("#name").val();
        if(val.length === 0){
        $("#error_name_message").text("Name is required");
        $("#error_name_message").show();
          isFormValidated = false;
        }
        else
        {
          $("#error_name_message").hide();
          isFormValidated = true;
        }
          return isFormValidated;
      }
      //Validate Arabic Name
      function validationAr_name()
      {
        let isFormValidated = false;
           let val = $("#er_name").val();
           if(val.length === 0){
            $("#error_er_name_message").text("مطلوب اسم");
            $("#error_er_name_message").show();
            isFormValidated = false;
        }else{
            $("#error_er_name_message").hide();
            isFormValidated = true;
        }
        return isFormValidated;
      }
    //Validate Manager Name
    function validateMName()
    {
        let isFormValidated = false;
        let val = $("#manager_name").val();
        if(val.length === 0){
            $("#error_m_name_message").text("Manager Name is required");
            $("#error_m_name_message").show();
            isFormValidated = false;
        }
        else
        {
            $("#error_name_message").hide();
            isFormValidated = true;
        }
        return isFormValidated;
    }

    function validateZone()
    {
      let isFormValidated = false;
      let val = $("#selectpicker-select").val();
      if(val.length === 0)
      {
        $("#error_zone_message").text("Zone is required");
        $("#error_zone_message").show();
        isFormValidated = false;
      }
      else
      {
        $("#error_zone_message").hide();
        isFormValidated = true;
      }
      return isFormValidated;
    }



//Validate Zone Area
    function validateZoneArea()
    {
      let isFormValidated = false;
      let kk = $(".yum").css('display');
      let val = $("#selectpicker-select1").val();
      if(kk === "none"){
        $("#error_zone_area_message").hide();
        isFormValidated = true;
      }else if(val.length === 0){
          $("#error_zone_area_message").text("Area is required");
          $("#error_zone_area_message").show();
          isFormValidated = false;
      }
      else
      {
        $("#error_zone_area_message").hide();
        isFormValidated = true;
      }
    return isFormValidated;
    }

// function validateEmailField(route){
//       return new Promise((resolve, reject) => {

//         let isFormValidated = false;
//      $("#email-input").value = $("#email-input").val();
//      let val = $("#email-input").val();
//      var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
//      let flag = regex.test(val);
//      if(!flag){
//          $("#error_email_message").text("Required Valid Email");
//          $("#error_email_message").show();
//          isFormValidated = false;
//          resolve(isFormValidated)
//      }else{
//         var email = $("#email-input").val();
//         //console.log(email); return false;
//         $.ajax({
          
//                 url: route,//"{{route('Restaurent.email')}}",
//                 method: 'POST',
//                 data:{email:email},
//                 type:"json",
//                 success:function(response)
//                 {
//                   console.log(email);
//                   if(response.status ==1)
//                   {
//                     $("#error_email_message").hide();
//                     isFormValidated = true;
//                     resolve(isFormValidated)
//                   }
//                   else
//                   {
//                     $("#error_email_message").text(response.msg);
//                      $("#error_email_message").show();
//                      isFormValidated = false;
//                      resolve(isFormValidated)
//                   }
//                 },
//                });
//         //

        
//      }
     

//       })
//  }

 function validateLocation()
 {
  let isFormValidated = false;
    $("#autocomplete_search").value = $("#autocomplete_search").val();
    let val = $("#autocomplete_search").val();
    if(val === "")
    {
        $("#error_location_message").text("Restaurant Location is required.");
        $("#error_location_message").show();
        isFormValidated=false;
    }
    else{
        $("#error_location_message").hide();
        isFormValidated = true;
    }
    return isFormValidated;
}


function validateAddress()
 {
                let isFormValidated = false;
    $("#address").value = $("#address").val();
    let val = $("#address").val();
    if(val === "")
    {
        $("#error_address_message").text("Address is required.");
        $("#error_address_message").show();
        isFormValidated=false;
    }
    else{
        $("#error_location_message").hide();
        isFormValidated = true;
    }
    return isFormValidated;
}
//Validation Function For Moblie Number
function validatePhoneNumber()
{
   let isFormValidated = false;
   $("#numbers-Only").value = $("#numbers-Only").val();
   let val = $("#numbers-Only").val();
   //var regex = /^[569]\d{7}$/;
   //var regex = /^[0-9]\d{7}$/;
   var regex =/^[0-9]{8,16}$/;
   //var regex = /^[0-9]*$/;
   //let flag = regex.test(val) && (val.length === 8);
   let flag = regex.test(val);
   console.log(flag);
   if(!flag){
       $("#error_mobile_message").text("Contact Number is required");
       $("#error_mobile_message").show();
       isFormValidated = false;
   }else{
       $("#error_mobile_message").hide();
       isFormValidated = true;
   }
   return isFormValidated;
}

//Validation Function For Manager Moblie Number
function validateManagerPhoneNumber()
{

   let isFormValidated = true;
   $("#managers-numbers-Only").value = $("#managers-numbers-Only").val();
   let val = $("#managers-numbers-Only").val();
   if(val != "")
   {

       var regex =/^[0-9]{8,16}$/;
       let flag = regex.test(val);
       if(!flag)
       {
           $("#error_m_mobile_message").text("Contact Number is required");
           $("#error_m_mobile_message").show();
           isFormValidated = false;
       }else
       {
           $("#error_m_mobile_message").hide();
           isFormValidated = true;
       }

   }
   else
   {
    $("#error_m_mobile_message").hide();
           isFormValidated = true;
   }
   
   return isFormValidated;
}

function logovalidate(){
                let isFormValidated = false;
  var file= $("#logo").val();
  var reg = /(.*?)\.(jpg|bmp|jpeg|png)$/;
  if(!file){
    $("#error_file_message").text("Please Select logo.");
    $("#error_file_message").show();
    isFormValidated = false;
}else if(!file.match(reg))
{
    $("#error_file_message").text("Invalid Format");
    $("#error_file_message").show();
    isFormValidated = false;
}else
{
    $("#error_file_message").hide();
    isFormValidated = true;
}
return isFormValidated;
}
function covervalidate(){
                let isFormValidated = false;
  var file= $("#cover_pic").val();
  var reg = /(.*?)\.(jpg|bmp|jpeg|png)$/;
  if(!file){
    $("#error_file1_message").text("Please select Cover Pic.");
    $("#error_file1_message").show();
    isFormValidated = false;
}
else if(!file.match(reg))
{
    $("#error_file1_message").text("Invalid Format.");
    $("#error_file1_message").show();
    isFormValidated = false;
}else
{
    $("#error_file1_message").hide();
    isFormValidated = true;
}
return isFormValidated;
}


function ValidateAdmin(){
    let isFormValidated = false;
    let val = $("#admin_charge").val();
    var regex = /^[0-9]*$/;
    //let flag = regex.test(val);
    let flag = regex.test(val) && (val > 0) && (val < 100);
 if(!flag){
    $("#error_admin_message").text("required valid Admin Commision");
    $("#error_admin_message").show();
    isFormValidated = false;
}else{
    $("#error_admin_message").hide();
    isFormValidated = true;
}
return isFormValidated;
}

$("input[value=Submit]").click(async function(event) {
    // if(isFormValidated){
    //     $(this).attr("disabled",true);
    //    $('#formdata').submit();
    // }else{
    //     alert("Are u baklol?");
    // }
    let a = [];
    a.push(validateName());
    a.push(validationAr_name());
    a.push(validateMName());
    a.push(await validateEmailField("{{route('Restaurent.email')}}"));
    a.push(validateLocation());
    a.push(validatePhoneNumber());
    a.push(validateManagerPhoneNumber());
    // a.push(ValidateDelivery());
    a.push(ValidateAdmin());
    a.push(validateZoneArea());
    //a.push(validateAr_about());
    a.push(validateZone());
    a.push(logovalidate());
    a.push(covervalidate()); 
    if(a.every(function(val){
        return(val === true);
    }) ){
        $('#formdata').submit();
    }
});
