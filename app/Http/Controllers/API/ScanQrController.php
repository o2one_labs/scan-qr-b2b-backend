<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ScanQr;

class ScanQrController extends Controller
{
    public function index(Request $request){

    	$data = ScanQr::get();
    	return response()->json(['status'=>1,'message'=>'Success','data'=>$data]);
    }
}
