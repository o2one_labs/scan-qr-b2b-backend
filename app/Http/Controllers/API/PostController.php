<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Post;
use App\PostDetail;

use Validator, DateTime, Mail, DB,Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PostController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }
      
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{ 
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'datentime' => 'required',
                ]
            ); 
            if ($validation->fails()) {
                return response(array('success' => 0, 'statuscode' => 400, 'msg' =>
                    $validation->getMessageBag()->first()), 400);
            } 

            $post = new Post();
            $post->user_name = $request->name;
            $post->datentime = $request->datentime;
            $post->save();
            foreach($request->photo as $key=>$value){
                $postDetail = new PostDetail();
                $postDetail->post_id = $post->id;
                $postDetail->photo = $value;
                $postDetail->photo_type = $request->photo_type[$key];
                $postDetail->comments = $request->comments[$key];
                $postDetail->save();
            }
            return response(['success' => 1, 'statuscode' => 200, 'msg' =>"Success"], 200);
        } catch (\Exception $e) {
            return response(['success' => 0, 'statuscode' => 500, 'msg' => $e->getMessage()], 500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
