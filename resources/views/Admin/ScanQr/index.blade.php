@extends('layouts.apphome')
@section('content')

            <div class="breadcrumb">
                <h1>Datatables</h1>
                <ul>
                    <li><a href="">UI Kits</a></li>
                    <li>Datatables</li>
                </ul>
            </div>
            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-6">
                    <h4>QR Code List</h4>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#myModal2" style="float:right;">Add</button>
                </div>
            </div>
            <div class="row">
<div class="col-md-12 mb-4">

    <div class="card text-left">

        <div class="card-body">

            <div class="table-responsive" style="width: 100%;">
                    <table id="user_table" class="table table-bordered table-striped display nowrap">
                     <thead>
                      <tr>
                        <!-- <th><input type="checkbox" class="checkall" id="0"></th> -->
                        <th>Name</th>
                        <th>Created On</th>             
                        <th>Action</th>             

                      </tr>
                     </thead>
                    </table>
            </div>

        </div>
    </div>
</div>
<div class="modal inmodal modal-form2 modal-bg" id="myModal2" role="dialog" aria-hidden="false" >
    <div class="modal-dialog">
        <form method="post" action="" enctype="multipart/form-data" id="imageUploadForm2" action="{{route('scan.store')}}">
            <div class="modal-content animated swing">
                <div class="modal-header">
                    <h4 class="modal-title" id="package_title">Add QR Code</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    
                </div>
                <div class="alert alert-danger error_popup hide" role="alert"><span></span>
                    <div class="remove_parent" style="float:right;cursor: pointer;">x</div>
                </div>
                <div class="modal-body" id="package_schedule">
                      <form class="needs-validation" id="formdata" novalidate method="POST" enctype="multipart/form-data" autocomplete="off">
               @csrf
               <div class="form-row">
                  <div class="col-md-6 mb-2">
                     <label for="validationTooltip01">Name<span style="font-size:14px;color:#ff005e;">*</span></label>
                     <input type="text" id="name" multiple class="form-control" placeholder="Name" name="name" data-required="true" data-parsley-required-message="Name is required" required>
                  </div>
                  <div class="col-md-6 mb-2">
                     <label for="validationTooltip02"><span style="font-size:14px;color:#ff005e;">*</span>Code</label>
                     <input type="text" id="gov_name_ar" multiple class="form-control" placeholder="Code" name="code" data-required="true" data-parsley-required-message="Code is required" required>
                  </div>
               </div>
               <button class="btn btn-sm btn-primary btn-gradient-info mr-2 import_button_label" id="form_submit" type="submit" style="margin-top:22px;">Submit</button> <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
            </form>
                </div>
                <div class="modal-footer"> 
                    
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal inmodal modal-form2 modal-bg" id="myModal" role="dialog" aria-hidden="false" >
    <div class="modal-dialog">
        <form method="post" enctype="multipart/form-data" id="imageUploadForm" action="{{route('scan.update')}}">
            <div class="modal-content animated swing">
                <div class="modal-header">
                    <h4 class="modal-title" id="package_title">Add QR Code</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    
                </div>
                <div class="alert alert-danger error_popup hide" role="alert"><span></span>
                    <div class="remove_parent" style="float:right;cursor: pointer;">x</div>
                </div>
                <div class="modal-body" id="package_schedule">
                      <form class="needs-validation" id="formdata" novalidate method="POST" enctype="multipart/form-data" autocomplete="off">
               @csrf
               <input type="hidden" name="id" value="" id="update_id">
               <div class="form-row">
                  <div class="col-md-6 mb-2">
                     <label for="validationTooltip01">Name<span style="font-size:14px;color:#ff005e;">*</span></label>
                     <input type="text" id="update_name" multiple class="form-control" placeholder="Name" name="name" data-required="true" data-parsley-required-message="Name is required" required>
                  </div>
                  <div class="col-md-6 mb-2">
                     <label for="validationTooltip02"><span style="font-size:14px;color:#ff005e;">*</span>Code</label>
                     <input type="text" id="update_code" multiple class="form-control" placeholder="Code" name="code" data-required="true" data-parsley-required-message="Code is required" required>
                  </div>
               </div>
               <button class="btn btn-sm btn-primary btn-gradient-info mr-2 import_button_label" id="form_submit" type="submit" style="margin-top:22px;">Submit</button> <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
            </form>
                </div>
                <div class="modal-footer"> 
                    
                </div>
            </div>
        </form>
    </div>
</div>

</div>

@endsection

@section('scripts')
<script src="{{asset('js/vendor/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/dataTables.checkboxes.min.js')}}"></script>
<script src="{{asset('js/dataTables.checkboxes.js')}}"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js"></script>

<script>
function paginationStart(number,url){ 
        $('#second_block').addClass('hide'); 
        $('#fourth_block').addClass('hide'); 
        let custom_url='';
        if(url==''){
            custom_url ="{{ route('scan.list') }}"+"?page="+number;
        }else{
            custom_url =url;
        }
    $.ajax({
            url: custom_url,
            type: "GET", 
            success: function(res) {
                console.log(res)
                if(res.statuscode==200){  
                console.log(res);  
                   let json =  res.influencer.data; 
                    $(function () {
                            var content = '';

                            for (var i = 0; i < json.length; i++) {
                            content += '<tr id="tr_' + json[i].id + '">';
                            content += '<td><center>' +(i+1)+ '</center></td>';
                            content += '<td><center>' +json[i].user_name+ '</center></td>';
                            content += '<td><center>' +json[i].created_at+ '</center></td>';
                            
                           // content += '<td><center><button type="button" class="btn-outline-secondary" onClick="edit_entitiy(' + json[i].id + ');Img_option_refresh();">Edit</button><button type="button" class="btn-outline-secondary" onClick="delete_entitiy(' + json[i].id + ')">Delete</button></center></td>';
                        //    content += '<td><a href="#" class="edit">Edit</a> <a href="#" class="delete">Delete</a></td>';
                            content += '</tr>';
                            } 
                            $('#categoriesTable tbody').html(content);  
                                    //bind Pagination
                        $('#first_block a').attr('go_to_url',res.influencer.first_page_url);  
                        $('#fifth_block a').attr('go_to_url',res.influencer.last_page_url);  
                        let current_page = res.influencer.current_page;
                        let total_page= Math.ceil(res.influencer.total/res.influencer.per_page);
    if(current_page>1){
        $('#second_block a').attr('go_to_page',res.influencer.current_page-1); 
        $('#second_block a').html(res.influencer.current_page-1); 
        $('#second_block').removeClass('hide'); 
    }
                        $('#third_block a').attr('go_to_page',res.influencer.current_page); 
                        $('#third_block a').html(res.influencer.current_page);    
                        $('#third_block').addClass('active'); 
    if(current_page<total_page){
        $('#fourth_block a').attr('go_to_page',res.influencer.current_page+1); 
        $('#fourth_block a').html(res.influencer.current_page+1); 
        $('#fourth_block').removeClass('hide'); 
    } 
                    });   
                }
            },
            error: function(xhr){ 
            }
        }); 
    } 
    $( document ).ready(function() {
        console.log('hello');
        // paginationStart(1,'');
    });

     table = $('#user_table').on( 'init.dt', function () {
     $("#user_table thead tr th:first").removeClass('sorting_asc');
 } ).DataTable({
  processing: true,
  serverSide: true,
  orderable: false,
  ajax: {
   url: "{{ route('scan.list') }}",
  },

   columns: [
   // {
   //  data: 'check',
   //  name: 'check',
   //  orderable: true,
   //  "width" : "20%",
   //  },

   {
    data: 'name',
    name: 'name',
    orderable: true,
    "width" : "20%",
    },

   {
       data: 'code',
       name: 'code',
       orderable: true,
       "width" : "20%",
   },
   {
       data: 'action',
       name: 'action',
       orderable: true,
       "width" : "20%",
   },
  ],
  // columnDefs: [
          
  //         { targets: 4, "width": "16%", render: $.fn.dataTable.render.moment( 'Do MMM YYYY' ) },
          
  //    ]
 });


 $(document).on('click', '.delete', function(){
  id = $(this).data('id');
  str ="{{ route('admin_post.destroy',['id'=>'replace' ]) }}"; 
 custom_url=str.replace('replace',id); 
  swal({
        title: 'Are you sure?',
        text: "You want to delete ",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'OK ',
        buttons: {
          cancel: {
            text: "Cancel",
            value: null,
            visible: true,
            className: "btn btn-warning",
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-info",
            closeModal: true,
          }
        }

      }).then(function(isConfirm){
        if(isConfirm){
          $.ajaxSetup({
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                         }
                     });
          $.ajax({
            url:custom_url,
            method: 'GET',
            type:"json",
            success: function(data) {
                swal("Deleted!", "Records has been Deleted", "success");
                $('#user_table').DataTable().ajax.reload();

            },
            error:function(){
              swal({
                text: 'Something went wrong',
                button: {
                  text: "OK",
                  value: true,
                  visible: true,
                  className: "btn btn-primary"
                }
              })
            }
        });
        }
      })

 });

 function editScan(id){
  console.log(id);
  str ="{{ route('scan.edit',['id'=>'replace' ]) }}"; 
  custom_url=str.replace('replace',id); 
  $.ajax({
      url: custom_url,
      type: "GET", 
      success: function(res) {
        console.log(res);
          if(res.status==1){ 
              $('#update_id').val(res.data.id);
              $('#update_name').val(res.data.name);
              $('#update_code').val(res.data.code);
              
              //$('#modal-form2').modal('show',{backdrop: 'static', keyboard: false});
          }
      }
  });
  $('#myModal').modal('show');
 }


  $(document).on('click', '.delete', function(){
  id = $(this).data('id');
  str ="{{ route('scan.destroy',['id'=>'replace' ]) }}"; 
 custom_url=str.replace('replace',id); 
  swal({
        title: 'Are you sure?',
        text: "You want to delete ",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'OK ',
        buttons: {
          cancel: {
            text: "Cancel",
            value: null,
            visible: true,
            className: "btn btn-warning",
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-info",
            closeModal: true,
          }
        }

      }).then(function(isConfirm){
        if(isConfirm){
          $.ajaxSetup({
                       headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                         }
                     });
          $.ajax({
            url:custom_url,
            method: 'GET',
            type:"json",
            success: function(data) {
                swal("Deleted!", "Records has been Deleted", "success");
                $('#user_table').DataTable().ajax.reload();

            },
            error:function(){
              swal({
                text: 'Something went wrong',
                button: {
                  text: "OK",
                  value: true,
                  visible: true,
                  className: "btn btn-primary"
                }
              })
            }
        });
        }
      })

 });
</script>
@endsection